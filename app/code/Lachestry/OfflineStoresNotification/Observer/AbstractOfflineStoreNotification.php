<?php

namespace Lachestry\OfflineStoresNotification\Observer;

use Lachestry\Telegram\Model\Api\TelegramProvider;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AbstractOfflineStoreNotification implements ObserverInterface
{
    protected $telegramProvider;

    public function __construct(TelegramProvider $telegramProvider)
    {
        $this->telegramProvider = $telegramProvider;
    }

    public function execute(Observer $observer)
    {
        $this->telegramProvider->sendMessage($this->getMessage($observer));

        return true;
    }

    protected function getMessage(Observer $observer)
    {
        return "Сообщение";
    }
}
