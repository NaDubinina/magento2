<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresNotification\Observer;

use Magento\Framework\Event\Observer;

class OfflineStoreDeleteNotification extends AbstractOfflineStoreNotification
{
    protected function getMessage(Observer $observer)
    {
        $offlineStore = $observer->getEvent()->getData('offlineStore');
        $message = "Внимание! Оффлайн магазин <b>{$offlineStore->getTitle()}</b>, Id: <b>{$offlineStore->getid()}</b> был удалён";

        return $message;
    }
}
