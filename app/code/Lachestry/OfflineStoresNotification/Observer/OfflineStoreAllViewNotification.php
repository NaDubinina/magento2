<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresNotification\Observer;

use Magento\Framework\Event\Observer;

class OfflineStoreAllViewNotification extends AbstractOfflineStoreNotification
{
    protected function getMessage(Observer $observer)
    {
        return "Внимание! Пытаются перейти на страницу со <b>всеми</b> оффлайн магазинами";
    }
}
