<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresNotification\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;

class NotifierType extends Select
{
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    public function setInputId($value)
    {
        return $this->setId($value);
    }

    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }

    private function getSourceOptions()
    {
        return [
            ['value' => 'telegram', 'label' => __('Telegram')],
            ['value' => 'email', 'label' => __('Email')]
        ];
    }
}
