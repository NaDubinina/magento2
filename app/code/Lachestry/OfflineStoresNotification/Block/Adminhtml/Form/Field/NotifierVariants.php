<?php

namespace Lachestry\OfflineStoresNotification\Block\Adminhtml\Form\Field;

class NotifierVariants extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    public const FIELD_NOTIFIER_TYPE = 'notifier_type';
    public const FIELD_RECIPIENT = 'recipient';

    /** @var NotifierType $typeRenderer */
    protected $typeRenderer;

    protected function getTypeRenderer(): NotifierType
    {
        if (!$this->typeRenderer) {
            $this->typeRenderer = $this->getLayout()->createBlock(
                NotifierType::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
            $this->typeRenderer->setClass('notifier_type_select');
        }

        return $this->typeRenderer;
    }


    protected function _prepareToRender(): void
    {
        $this->addColumn(
            self::FIELD_NOTIFIER_TYPE,
            [
                'label' => __('Notifier'),
                'renderer' => $this->getTypeRenderer()
            ]
        );
        $this->addColumn(self::FIELD_RECIPIENT, ['label' => __('Recipient')]);

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add notifier');
    }


    protected function _prepareArrayRow(\Magento\Framework\DataObject $row): void
    {
        $notifierType = $row->getData(self::FIELD_NOTIFIER_TYPE);
        $notifier = $row->getData($notifierType);

        $optionExtraAttr = [];
        $option = $this->getTypeRenderer()->calcOptionHash($notifier);
        $optionExtraAttr['option_' . $option] = 'selected="selected"';

        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}
