<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\Model;

use Lachestry\OfflineStores\Model\DataHandlerDB;
use \Lachestry\OfflineStoresImportExport\Model\ResourceModel\Log as LogResource;

class Logger extends \Monolog\Logger implements \Psr\Log\LoggerInterface
{
    protected $dataHandlerDB;

    public function __construct(
        DataHandlerDB $dataHandlerDB,
        $name = 'logger offline stores',
        $handlers = [],
        $processors = []
    ) {
        parent::__construct($name, $handlers, $processors);
        $this->dataHandlerDB = $dataHandlerDB;
    }

    public function makeChangeInTableLog(string $type, string $tableName, string $status)
    {
        $this->info(
            ('change in ' . $tableName),
            [
                Log::FIELD_TYPE => $type,
                Log::FIELD_STATUS => $status,
            ]
        );

        $data = [
            Log::FIELD_TYPE => $type,
            Log::FIELD_STATUS => $status,
        ];

        $this->dataHandlerDB->updateDatabaseArrayEntityData(LogResource::TABLE_NAME, $data);
    }
}
