<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\Model\Api;

use Lachestry\OfflineStoresImportExport\Model\Api\Http\Client;
use Magento\Framework\HTTP\ZendClient;

class OfflineStoreProvider
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getOfflineStores()
    {
        return $this->client->request('http://localhost:3000/offlinestores');
    }

    public function postOfflineStore(\Lachestry\OfflineStores\Model\OfflineStore $offlineStore): bool
    {
        try {
            $this->client->setBodyData($offlineStore->getData());
            $this->client->request('http://localhost:3000/offlinestores', ZendClient::POST);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
