<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\Model\Api;

use Lachestry\OfflineStores\Model\Data\Employee;
use Lachestry\OfflineStoresImportExport\Model\Api\Http\Client;
use Magento\Framework\HTTP\ZendClient;

class EmployeeProvider
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getEmployees()
    {
        return $this->client->request('http://localhost:3000/employees');
    }

    public function postEmployee(array $employee): bool
    {
        try {
            $this->client->setBodyData($employee);
            $this->client->request('http://localhost:3000/employees', ZendClient::POST);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
