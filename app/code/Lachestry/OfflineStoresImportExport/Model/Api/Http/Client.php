<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\Model\Api\Http;

use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Serialize\Serializer\Json;

class Client
{
    protected $clientFactory;
    protected $jsonSerializer;
    protected $bodyData = null;

    public function  __construct(
        ZendClientFactory $clientFactory,
        Json $jsonSerializer
    ) {
        $this->clientFactory = $clientFactory;
        $this->jsonSerializer = $jsonSerializer;
    }

    public function setBodyData(array $data)
    {
        $this->bodyData = $data;
        return $this;
    }

    public function request(string $url, string $method = ZendClient::GET): array
    {
        $client = $this->buildClient();

        try {
            $client->setUri($url);

            if (isset($this->bodyData)) {
                $client->setRawData($this->jsonSerializer->serialize($this->bodyData));
            }

            $response = $client->request($method);

            $statusGroup = ($response->getStatus()) / 100;
            if ((int) $statusGroup != 2) {
                throw new \Exception('error');
            }

            $body = $response->getBody();
            return $this->jsonSerializer->unserialize($body);
        } catch (\Exception $e) {
            return [];
        }
    }

    private function  buildClient(): ZendClient
    {
        return $this->clientFactory->create();
    }
}
