<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\Model\Api;

use Lachestry\OfflineStores\Model\DataHandlerDB;
use Lachestry\OfflineStores\Model\ResourceModel\Employee;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore;
use Lachestry\OfflineStoresImportExport\Model\Log;
use Lachestry\OfflineStoresImportExport\Model\Logger;

class DataReconciliator
{
    protected $employeeProvider;
    protected $offlineStoreProvider;
    protected $dataHandlerDB;
    protected $offlineStoreCollection;
    protected $employeeCollection;

    public function __construct(
        EmployeeProvider $employeeProvider,
        OfflineStoreProvider $offlineStoreProvider,
        DataHandlerDB $dataHandlerDB,
        OfflineStore\Collection $offlineStoreCollection,
        Employee\Collection $employeeCollection,
        Logger $logger
    ) {
        $this->offlineStoreCollection = $offlineStoreCollection;
        $this->employeeCollection = $employeeCollection;
        $this->logger = $logger;
        $this->dataHandlerDB = $dataHandlerDB;
        $this->employeeProvider = $employeeProvider;
        $this->offlineStoreProvider = $offlineStoreProvider;
    }
    public function importOfflineStoreJsonDatabase()
    {
        $data = $this->offlineStoreProvider->getOfflineStores();
        $status = $this->dataHandlerDB->updateDatabaseArrayEntityData(OfflineStore::TABLE_NAME, $data);
        $this->recordSynchronizationData(Log::FIELD_TYPE_IMPORT, OfflineStore::TABLE_NAME, $status);
    }

    public function importEmployeeJsonDatabase()
    {
        $data = $this->employeeProvider->getEmployees();
        $status = $this->dataHandlerDB->updateDatabaseArrayEntityData(Employee::TABLE_NAME, $data);
        $this->recordSynchronizationData(Log::FIELD_TYPE_IMPORT, Employee::TABLE_NAME, $status);
    }

    public function exportOfflineStoreDatabaseJson()
    {
        $offlineStores = $this->offlineStoreCollection->getItems();
        $status = true;

        foreach ($offlineStores as $offlineStore) {
            if (!$this->offlineStoreProvider->postOfflineStore($offlineStore)) {
                $status = false;
            }
        }

        $this->recordSynchronizationData(Log::FIELD_TYPE_EXPORT, OfflineStore::TABLE_NAME, $status);
    }

    public function exportEmployeeDatabaseJson()
    {
        $employees = $this->employeeCollection->getData();
        $status = true;

        foreach ($employees as $employee) {
            if (!$this->employeeProvider->postEmployee($employee)) {
                $status = false;
            }
        }

        $this->recordSynchronizationData(Log::FIELD_TYPE_EXPORT, Employee::TABLE_NAME, $status);
    }

    private function recordSynchronizationData(string $type, string $tableName, bool $status)
    {
        $status = ($status) ? Log::FIELD_STATUS_SUCCESS : Log::FIELD_STATUS_FAILURE;

        $this->logger->makeChangeInTableLog($type, $tableName, $status);
    }
}