<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\Model\ResourceModel\Log;

use Lachestry\OfflineStoresImportExport\Model\Log;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * @method Log[] getItems()
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            Log::class,
            \Lachestry\OfflineStoresImportExport\Model\ResourceModel\Log::class
        );
    }
}
