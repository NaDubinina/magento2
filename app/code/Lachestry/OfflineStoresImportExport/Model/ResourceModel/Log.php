<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Lachestry\OfflineStoresImportExport\Model\Log as LogEntity;

class Log extends AbstractDb
{
    public const TABLE_NAME = 'lachestry_offline_store_import_export_log';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, LogEntity::FIELD_ID);
    }
}
