<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * @method string  getType()
 * @method string  getStatus()
 * @method string  getTableName()
 * @method string  getCreatedAt()
 * @method string  getUpdatedAt()
 */
class Log extends AbstractModel
{
    public const FIELD_ID = 'id';
    public const FIELD_TYPE = 'type';
    public const FIELD_STATUS = 'status';
    public const FIELD_CREATED_AT = 'created_at';
    public const FIELD_UPDATED_AT = 'updated_at';
    public const FIELD_TYPE_IMPORT = 'import';
    public const FIELD_TYPE_EXPORT = 'export';
    public const FIELD_STATUS_SUCCESS = 'success';
    public const FIELD_STATUS_FAILURE = 'failure';

    protected function _construct()
    {
        $this->_init(ResourceModel\Log::class);
    }
}
