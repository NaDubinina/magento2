<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStoresImportExport\ViewModel;

use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\CollectionFactory;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class OfflineStoreList implements ArgumentInterface
{
    protected $offlineStoreCollection;

    public function __construct(CollectionFactory $collection)
    {
        $this->offlineStoreCollection = $collection->create();
    }

    public function getOfflineStores()
    {
        return $this->offlineStoreCollection;
    }
}