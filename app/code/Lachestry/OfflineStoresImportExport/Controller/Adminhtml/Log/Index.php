<?php
declare(strict_types=1);

namespace Lachestry\OfflineStoresImportExport\Controller\Adminhtml\Log;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStoresImportExport::importexport';

    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
