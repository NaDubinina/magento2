<?php
declare(strict_types=1);

namespace Lachestry\OfflineStoresImportExport\Controller\Adminhtml\Log;

use Lachestry\OfflineStoresImportExport\Model\Api\DataReconciliator;
use Lachestry\OfflineStoresImportExport\Model\Log;
use Lachestry\OfflineStoresImportExport\Model\Logger;
use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStoresImportExport::importexport_save';
    protected $offlineStoreRepository;
    protected $offlineStoreFactory;
    protected $dataHandlerDB;

    public function __construct(
        Action\Context $context,
        DataReconciliator $reconciliator,
        Logger $logger
    ) {
        $this->reconciliator = $reconciliator;
        $this->logger = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        $type = $this->getRequest()->getParam(Log::FIELD_TYPE);
        $table = $this->getRequest()->getParam('table');
        $redirect = $this->resultRedirectFactory->create();

        if (!$this->getRequest()->isPost()) {
            return $redirect->setPath('*/*/index');
        }

        try {
            if ($type === Log::FIELD_TYPE_IMPORT) {
                $method = "import{$table}JsonDatabase";
                $this->reconciliator->$method();
            } else {
                $method = "export{$table}DatabaseJson";
                $this->reconciliator->$method();
            }
        } catch (\Exception $exception) {
            $this->logger->log($this->logger::ERROR, $exception->getMessage());
        }

        if ($this->getRequest()->getParam('back')) {
            return $redirect->setPath('*/*/index');
        }

        return $redirect->setPath('*/*/index');
    }
}
