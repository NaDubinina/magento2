<?php

namespace Lachestry\OfflineStoresImportExport\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Lachestry\OfflineStoresImportExport\Model\Log as LogEntity;
use \Lachestry\OfflineStoresImportExport\Model\ResourceModel\Log as LogResource;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()
            ->newTable($setup->getTable(LogResource::TABLE_NAME))
            ->addColumn(
                LogEntity::FIELD_ID,
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Id'
            )->addColumn(
                LogEntity::FIELD_TYPE,
                Table::TYPE_TEXT,
                6,
                ['nullable' => false],
                'Type: import/export'
            )->addColumn(
                LogEntity::FIELD_STATUS,
                Table::TYPE_TEXT,
                7,
                ['nullable' => false],
                'Status: success/failure'
            )->addColumn(
                LogEntity::FIELD_CREATED_AT,
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT
                ],
                'Date of import/export'
            )->addColumn(
                LogEntity::FIELD_UPDATED_AT,
                Table::TYPE_TIMESTAMP,
                null,
                [
                    'nullable' => false,
                    'default' => Table::TIMESTAMP_INIT_UPDATE
                ],
                'Date of import/export update'
            )->setComment(
                'Table of import or export logs'
            );

        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}
