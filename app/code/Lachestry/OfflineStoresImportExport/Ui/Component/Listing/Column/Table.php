<?php

namespace Lachestry\OfflineStoresImportExport\Ui\Component\Listing\Column;

class Table implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'OfflineStore', 'label' => __('Offline Stores Table')],
            ['value' => 'Employee', 'label' => __('Employees Table')]
        ];
    }
}
