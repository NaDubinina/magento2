<?php

namespace Lachestry\OfflineStoresImportExport\Ui\Component\Listing\Column;

class Type implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'import', 'label' => __('Import')],
            ['value' => 'export', 'label' => __('Export')]
        ];
    }
}