<?php

namespace Lachestry\OfflineStoresImportExport\Ui\Component\DataProvider;

use Lachestry\OfflineStoresImportExport\Model\ResourceModel\Log\Grid\CollectionFactory;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldData,
        CollectionFactory $collectionFactory,
        $meta = [],
        $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldData,
            $meta,
            $data
        );
        $this->collection = $collectionFactory->create();
    }

    public function getData()
    {
        return $this->collection->getData();
    }
}
