<?php

namespace Lachestry\OfflineStores\Controller\Adminhtml;

use Magento\Framework\Api\AbstractSimpleObject;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Backend\App\Action;

abstract class AbstractSave extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::offlinestore_save';
    const MEDIA_DIRECTORY = 'offlinestores/';

    protected $uploadFile;
    protected $fileSystem;
    protected $logger;

    public function __construct(
        Action\Context $context,
        FileSystem $fileSystem,
        UploaderFactory $uploadFile,
        \Magento\Catalog\Model\ImageUploader $imageUploader
    ) {
        $this->uploadFile = $uploadFile;
        $this->fileSystem = $fileSystem;
        $this->imageUploader = $imageUploader;
        parent::__construct($context);
    }

    protected function saveImage(AbstractSimpleObject $dataObject): bool
    {
        $image = $this->getRequest()->getFiles('image');

        if (!$image['size'] > 0) {
            return false;
        }

        $uploader = $this->uploadFile->create(['fileId' => $image]);
        $path = $this->fileSystem
            ->getDirectoryRead(DirectoryList::MEDIA)
            ->getAbsolutePath(static::MEDIA_DIRECTORY);

        $uploader->save($path);
        $imageName = $uploader->getUploadedFileName();

        $imageName = static::MEDIA_DIRECTORY . '/' . $imageName;
        if ($imageName) {
            $dataObject->setImage($imageName);
        }

        return true;
    }
}