<?php

namespace Lachestry\OfflineStores\Controller\Adminhtml\OfflineStore;

use Lachestry\OfflineStores\Model\OfflineStore;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;

class MassChangeIsActive extends Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::offlinestore';

    protected $filter;
    protected $offlineStoreCollectionFactory;

    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collection
   ) {
        $this->filter = $filter;
        $this->offlineStoreCollectionFactory = $collection;
        parent::__construct($context);
    }

    public function execute()
    {
        $isActiveValue = $this->getRequest()->getParam(OfflineStore::FIELD_IS_ACTIVE);
        $isActiveValue =  boolval($isActiveValue);

        $selectedIds = $this->getRequest()->getParam('selected');

        $collection = $this->offlineStoreCollectionFactory->create();
        $collection->addFieldToFilter(OfflineStore::FIELD_ID, ['in' => $selectedIds]);

        foreach ($collection->getItems() as $offlineStore) {
            try {
                $offlineStore->setIsActive($isActiveValue);
                $offlineStore->save();
            } catch (\Exception $exception) {

            }
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been modified.', count($selectedIds)));
       
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/index');
    }
}
