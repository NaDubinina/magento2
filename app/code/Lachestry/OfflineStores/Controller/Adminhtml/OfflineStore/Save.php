<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Controller\Adminhtml\OfflineStore;

use Lachestry\OfflineStores\Api\Data\OfflineStoreInterfaceFactory;
use Lachestry\OfflineStores\Api\Data\OfflineStoreInterface;
use Lachestry\OfflineStores\Api\OfflineStoreRepositoryInterface;
use Lachestry\OfflineStores\Controller\Adminhtml\AbstractSave;
use Lachestry\OfflineStores\Model\DataHandlerDB;
use Lachestry\OfflineStoresImportExport\Model\Logger;
use Magento\Backend\App\Action;
use \Lachestry\OfflineStores\Model\OfflineStore;
use Magento\Framework\FileSystem;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::offlinestore_save';
    protected $offlineStoreRepository;
    protected $offlineStoreFactory;
    protected $dataHandlerDB;

    public function __construct(
        Action\Context $context,
        OfflineStoreRepositoryInterface $offlineStoreRepository,
        OfflineStoreInterfaceFactory $offlineStoreFactory,
        DataHandlerDB $dataHandlerDB,
        Logger $logger
    ) {
        $this->dataHandlerDB = $dataHandlerDB;
        $this->logger = $logger;
        $this->offlineStoreRepository = $offlineStoreRepository;
        $this->offlineStoreFactory = $offlineStoreFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $offlineStoreId = $this->getRequest()->getParam(OfflineStore::FIELD_ID);
        $redirect = $this->resultRedirectFactory->create();

        if (!$this->getRequest()->isPost()) {
            return $redirect->setPath('*/*/index');
        }

        try {
            if ($offlineStoreId) {
                $offlineStore = $this->offlineStoreRepository->getById($offlineStoreId);
            }
        } catch ( \Exception $exception) {
            $this->logger->log($this->logger::ERROR, $exception->getMessage());

            $this->messageManager->addErrorMessage(__('Offline Store ID not exist'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }

        if (!isset($offlineStore)) {
            $offlineStore = $this->buildOfflineStoreData();
        }

        if (!$this->saveUpdateOfflineStore($offlineStore, $data['content'])) {
            return $redirect->setPath('*/*/edit', [OfflineStore::FIELD_ID => $offlineStoreId]);
        }

        if ($this->getRequest()->getParam('back')) {
            return $redirect->setPath('*/*/edit', [OfflineStore::FIELD_ID => $offlineStoreId]);
        }

        return $redirect->setPath('*/*/index');
    }

    protected function buildOfflineStoreData (): OfflineStoreInterface
    {
        return $this->offlineStoreFactory->create();
    }

    protected function saveUpdateOfflineStore($offlineStore, $data)
    {
        try {
            $offlineStore
                ->setTitle($data[OfflineStore::FIELD_TITLE])
                ->setMetaTitle($data[OfflineStore::FIELD_META_TITLE])
                ->setEmployeeQty($data[OfflineStore::FIELD_EMPLOYEE_QTY])
                ->setStoreId($data[OfflineStore::FIELD_STORE_ID])
                ->setAverageSalary($data[OfflineStore::FIELD_AVERAGE_SALARY])
                ->setCity($data[OfflineStore::FIELD_CITY])
                ->setSlug($data[OfflineStore::FIELD_SLUG])
                ->setLat($data[OfflineStore::FIELD_LAT])
                ->setLng($data[OfflineStore::FIELD_LNG])
                ->setIsActive($data[OfflineStore::FIELD_IS_ACTIVE]);

            if (isset($data[OfflineStore::FIELD_IMAGE]['delete'])) {
                $offlineStore->setImage(NULL);
            } else {
                $offlineStore->setImage($data[OfflineStore::FIELD_IMAGE][0]['path']);
            }

            $this->offlineStoreRepository->save($offlineStore);

            return true;
        } catch (\Exception $exception) {
            $this->logger->log($this->logger::ERROR, $exception->getMessage());
            return false;
        }
    }
}
