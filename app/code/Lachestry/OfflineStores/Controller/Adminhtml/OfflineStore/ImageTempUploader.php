<?php

declare(strict_types=1);

namespace Lachestry\OfflineStores\Controller\Adminhtml\OfflineStore;


use Lachestry\OfflineStores\Model\OfflineStore;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Store\Model\StoreManagerInterface;

class ImageTempUploader extends \Magento\Backend\App\Action
{
    protected $imageUploader;
    protected $imageDir = 'offlinestores/offlinestore';

    public function __construct(
        \Magento\Backend\App\Action\Context  $context,
        UploaderFactory $imageUploader,
        FileSystem $fileSystem,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
        $this->mediaDirectory = $fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->storeManager = $storeManager;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Lachestry_OfflineStores::offlinestores');
    }

    public function execute(): ResultInterface
    {
        $jsonResult = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $imageId = $this->_request->getParam('param_name', 'image');
        try {
            $this->imageUploader = $this->imageUploader->create(['fileId' => $imageId]);
            $this->imageUploader->setAllowedExtensions(['jpg', 'jpeg', 'png']);
            $this->imageUploader->setAllowRenameFiles(true);
            $this->imageUploader->setAllowCreateFolders(true);
            $this->imageUploader->setFilesDispersion(false);

            $path = $this->mediaDirectory->getAbsolutePath($this->imageDir);
            $result = $this->imageUploader->save($path);

            $media = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
            $result['url'] = $media . $this->imageDir . '/' . $this->imageUploader->getUploadedFileName();
            $result['path'] = $this->imageDir . '/' . $this->imageUploader->getUploadedFileName();

        } catch (\Exception $e) {
            $result = ['errorcode' => $e->getCode(), 'error' => $e->getMessage()];
        }

        return $jsonResult->setData($result);
    }
}
