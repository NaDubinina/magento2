<?php
namespace Lachestry\OfflineStores\Controller\Adminhtml\OfflineStore;

use Lachestry\OfflineStores\Model\OfflineStore;
use Magento\Backend\App\Action\Context;
use Lachestry\OfflineStores\Api\OfflineStoreRepositoryInterface as OfflineStoreRepository;
use Magento\Framework\Controller\Result\JsonFactory;
use Lachestry\OfflineStores\Api\Data\OfflineStoreInterface;

class InlineEdit extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::lachestry_offlinestores';
    protected $offlineStoreRepository;
    protected $jsonFactory;

    public function __construct(
        Context                $context,
        OfflineStoreRepository $offlineStoreRepository,
        JsonFactory            $jsonFactory
    ) {
        parent::__construct($context);
        $this->offlineStoreRepository = $offlineStoreRepository;
        $this->jsonFactory = $jsonFactory;
    }

    public function execute()
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $offlineStores = $this->getRequest()->getParam('items', []);
            if (!count($offlineStores)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach ($offlineStores as $offlineStoreReq) {
                    try {
                        $offlineStore = $this->offlineStoreRepository->getById($offlineStoreReq[OfflineStore::FIELD_ID]);

                        foreach ($offlineStoreReq as $field => $value) {
                            $offlineStore->setData($field, $value);
                        }

                        $this->offlineStoreRepository->save($offlineStore);
                    } catch (\Exception $e) {
                        $messages[] = $this->getErrorWithBlockId(
                            $offlineStore,
                            __($e->getMessage())
                        );
                        $error = true;
                    }
                }

            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    protected function getErrorWithBlockId(OfflineStoreInterface $offlineStore, $errorText)
    {
        return '[Offline Store ID: ' . $offlineStore->getId() . '] ' . $errorText;
    }
}
