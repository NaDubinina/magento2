<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Controller\Adminhtml\OfflineStore;

use Lachestry\OfflineStores\Model\OfflineStore;
use Lachestry\OfflineStores\Model\OfflineStoreFactory;
use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::offlinestores_edit';

    protected $coreRegistry;
    protected $resultPageFactory;
    protected $offlineStoreFactory;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        OfflineStoreFactory $offlineStoreFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->offlineStoreFactory = $offlineStoreFactory->create();

        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam(OfflineStore::FIELD_ID);
        $model = $this->offlineStoreFactory;

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('Offline Store not exist'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->coreRegistry->register('offlineStores_offlinestore', $model);

        $breadCrumb = $id ? __('Edit Offline store') : __('New Offline store');

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb($breadCrumb, $breadCrumb);
        $resultPage->getConfig()->getTitle()->prepend(__('Offline store'));
        $resultPage
            ->getConfig()
            ->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Offline store'));

        return $resultPage;
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage
            ->setActiveMenu('Lachestry_OfflineStores::lachestry')
            ->addBreadcrumb(__('Offline store'), __('Offline store'))
            ->addBreadcrumb(__('Manage Offline store'), __('Manage Offline store'));

        return $resultPage;
    }
}

