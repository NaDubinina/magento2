<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Controller\Adminhtml\OfflineStore;

use Magento\Framework\Controller\ResultFactory;

class NewAction extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::offlinestore_create';

    public function execute()
    {
        $this->resultFactory->create(ResultFactory::TYPE_FORWARD)->forward('edit');
    }
}

