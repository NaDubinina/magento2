<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Controller\Adminhtml\OfflineStore;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::offlinestores';

    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
