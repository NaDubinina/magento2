<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Controller\Adminhtml\Employee;

use Lachestry\OfflineStores\Model\Employee;
use Lachestry\OfflineStores\Model\EmployeeFactory;
use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::employee_edit';

    protected $coreRegistry;
    protected $resultPageFactory;
    protected $employeeFactory;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        EmployeeFactory $employeeFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        $this->employeeFactory = $employeeFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('employee_id');
        $model = $this->employeeFactory->create();

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('Employee not exist'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->coreRegistry->register('offlineStores_employee', $model);

        $breadCrumb = $id ? __('Edit Employee') : __('New Employee');

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb($breadCrumb, $breadCrumb);
        $resultPage->getConfig()->getTitle()->prepend(__('Employees'));
        $resultPage
            ->getConfig()
            ->getTitle()
            ->prepend($model->getId() ? $model->getTitle() : __('New Employee'));

        return $resultPage;
    }

    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage
            ->setActiveMenu('Lachestry_OfflineStores::employee')
            ->addBreadcrumb(__('Employee'), __('Employee'))
            ->addBreadcrumb(__('Manage Employee'), __('Manage Employee'));

        return $resultPage;
    }
}

