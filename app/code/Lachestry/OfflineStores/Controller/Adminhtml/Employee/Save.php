<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Controller\Adminhtml\Employee;

use Lachestry\OfflineStores\Api\Data\EmployeeInterface;
use Lachestry\OfflineStores\Api\Data\EmployeeInterfaceFactory;
use Lachestry\OfflineStores\Api\EmployeeRepositoryInterface;
use Lachestry\OfflineStores\Controller\Adminhtml\AbstractSave;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore as OfflineStoreResource;
use Lachestry\OfflineStoresImportExport\Model\Logger;
use Magento\Backend\App\Action;
use \Lachestry\OfflineStores\Model\Employee;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;

class Save extends AbstractSave
{
    const ADMIN_RESOURCE = 'Lachestry_OfflineStores::employee_save';
    const MEDIA_DIRECTORY = 'offlinestores/employee';

    protected $coreRegistry;
    protected $resultPageFactory;
    protected $employeeRepository;
    protected $employeeFactory;
    protected $logger;

    public function __construct(
        Action\Context $context,
        EmployeeRepositoryInterface $employeeRepository,
        EmployeeInterfaceFactory $employeeFactory,
        FileSystem $fileSystem,
        UploaderFactory $uploadFile,
        Logger $logger,
        \Magento\Catalog\Model\ImageUploader $imageUploader
    ) {
        $this->logger = $logger;
        $this->employeeRepository = $employeeRepository;
        $this->employeeFactory = $employeeFactory;

        parent::__construct($context, $fileSystem,$uploadFile, $imageUploader);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $employeeId = $this->getRequest()->getParam(Employee::FIELD_ID);
        $redirect = $this->resultRedirectFactory->create();

        if (!$this->getRequest()->isPost()) {
            return $redirect->setPath('*/*/index');
        }

        try {
            if ($employeeId) {
                $employee = $this->employeeRepository->getById($employeeId);
            }
        } catch ( \Exception $exception) {
            $this->logger->log($this->logger::ERROR, OfflineStoreResource::TABLE_NAME);
        }

        if (!isset($employee)) {
            $employee = $this->buildEmployeeData();
        }

        if (!$this->saveUpdateEmployee($employee, $data)) {
            return $redirect->setPath('*/*/edit', ['employee_id' => $employeeId]);
        }

        if ($this->getRequest()->getParam('back')) {
            return $redirect->setPath('*/*/edit', ['employee_id' => $employeeId]);
        }


        return $redirect->setPath('*/*/index');
    }

    protected function buildEmployeeData (): EmployeeInterface
    {
        return $this->employeeFactory->create();
    }

    protected function saveUpdateEmployee($employee, $data)
    {
        try {
            $employee
                ->setFirstName($data[Employee::FIELD_FIRST_NAME])
                ->setLastName($data[Employee::FIELD_LAST_NAME])
                ->setDob($data[Employee::FIELD_DOB])
                ->setSalary($data[Employee::FIELD_SALARY])
                ->setOfflineStoreId($data[Employee::FIELD_OFFLINE_STORE_ID])
                ->setIsActive($data[Employee::FIELD_IS_ACTIVE]);

            if (isset($data[Employee::FIELD_IMAGE]['delete'])) {
                $employee->setImage(NULL);
            } else {
                $this->saveImage($employee);
            }

            $this->employeeRepository->save($employee);

            return true;
        } catch (\Exception $exception) {
            $this->logger->log($this->logger::ERROR,  $exception->getMessage());
            return false;
        }
    }
}

