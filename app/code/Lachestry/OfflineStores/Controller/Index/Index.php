<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Controller\Index;

use Lachestry\OfflineStores\Block\OfflineStore\OfflineStoresList;
use Lachestry\OfflineStores\Api\OfflineStoreRepositoryInterface as OfflineStoreRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $pageFactory;
    protected $offlineStoreRepository;
    protected $searchCriteriaBuilder;
    protected $events;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        OfflineStoreRepository $offlineStoreRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ManagerInterface $events
    ) {
        parent::__construct($context);

        $this->events = $events;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->offlineStoreRepository = $offlineStoreRepository;
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->setPageSize(2)
            ->create();
        $offlineStores = $this->offlineStoreRepository
            ->getList($searchCriteria)
            ->getItems();

        $page = $this->pageFactory->create();
        $block = $this->getOfflineStoresBlock($page);


        if ($block) {
            $block->setOfflineStores($offlineStores);
        }

        $this->events->dispatch(
            'lachestry_offline_store_all_viewing'
        );

        return $page;
    }

    protected function getOfflineStoresBlock(Page $pageResult): ?OfflineStoresList
    {
        return $pageResult->getLayout()->getBlock(OfflineStoresList::BLOCK_NAME) ?: null;
    }
}