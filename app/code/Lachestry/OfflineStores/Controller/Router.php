<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Controller;

use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\Collection;
use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\Exception\NotFoundException;

class Router implements RouterInterface
{
    public const FRONT_NAME = 'offlinestores';
    public const CONTROLLER_NAME = 'offlinestore';
    public const PATH_INDEX_MODULE = 0;
    public const PATH_INDEX_STORE_SLUG = 1;

    protected $actionFactory;
    protected $isProcessed = false;

    public function __construct(ActionFactory $actionFactory, Collection $collection)
    {
        $this->actionFactory = $actionFactory;
        $this->collection = $collection;
    }

    /**
     * Validate and Match Cms Page and modify request
     *
     * @param \Magento\Framework\App\Request\Http|\Magento\Framework\App\RequestInterface $request
     * @return \Magento\Framework\App\ActionInterface|null
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $storeIds = [];
        $stores = $this->collection->getItems();

        foreach ($stores as $store) {
            $storeIds[$store->getSlug()] = $store->getId();
        }

        if ($this->isProcessed) {
            return null;
        }

        $pathInfo = explode('/', trim($request->getPathInfo(), '/'));
        $frontName = $pathInfo[self::PATH_INDEX_MODULE] ?? null;
        $slug = $pathInfo[self::PATH_INDEX_STORE_SLUG] ?? null;
        if ($frontName !== self::FRONT_NAME || !$slug) {
            return null;
        }

        $storeId = $storeIds[$slug] ?? null;
        if (!$storeId || $slug === self::CONTROLLER_NAME) {
            return null;
        }

        $request
            ->setModuleName('offlinestores')
            ->setControllerName('offlinestore')
            ->setActionName('view')
            ->setParam('id', $storeId);

        $this->isProcessed = true;
        return $this->actionFactory->create(Forward::class);
    }
}
