<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Controller\OfflineStore;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Lachestry\OfflineStores\Block\OfflineStore\View as ViewBlock;

class View extends Action
{
    protected $pageFactory;
    protected $offlineStore;

    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);

        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        $id =  (int) $this->getRequest()->getParam('id');
        $pageResult = $this->pageFactory->create();

        $block = $this->getOfflineStoreBlock($pageResult);
        if ($block && $id) {
            $offlineStore = $this->offlineStore->load($id);

            if (!$offlineStore->getId()) {
                throw new NotFoundException(__('Offline Store not found'));
            }

            $block->setOfflineStore($offlineStore);
            $pageResult->getConfig()->getTitle()->set($offlineStore->getMetaTitle());
        }

        $this->_eventManager->dispatch(
            'lachestry_offline_store_viewing',
            [
                'pageUrl' => $this->_url->getCurrentUrl(),
                'offlineStore' => $offlineStore,
            ]
        );
        return $pageResult;
    }

    protected function getOfflineStoreBlock(Page $pageResult): ?ViewBlock
    {
        return $pageResult->getLayout()->getBlock(ViewBlock::BLOCK_NAME) ?: null;
    }
}

