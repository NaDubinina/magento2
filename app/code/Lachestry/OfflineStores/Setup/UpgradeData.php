<?php

namespace Lachestry\OfflineStores\Setup;

use Lachestry\OfflineStores\Model\DataHandlerDB;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore as OfflineStoreResource;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Module\Dir;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    public const DATA_OFFLINE_STORE_JSON = 'update-store-data.json';
    private $dirModule;
    private $json;
    private $fileReader;
    private $dataHandlerDB;

    public function __construct(
        Dir $dirModule,
        Json $json,
        File $fileReader,
        DataHandlerDB $dataHandlerDB
   ) {
        $this->dataHandlerDB = $dataHandlerDB;
        $this->fileReader = $fileReader;
        $this->json = $json;
        $this->dirModule = $dirModule;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->insertAdditionalData($setup);
        }
    }

    protected function insertAdditionalData(ModuleDataSetupInterface $setup)
    {
        $modulePath = $this->dirModule->getDir('Lachestry_OfflineStores');
        $dirDataPath = $modulePath . DIRECTORY_SEPARATOR . 'Setup' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;

        $data = $this->fileReader->fileGetContents($dirDataPath. DIRECTORY_SEPARATOR . self::DATA_OFFLINE_STORE_JSON);
        $data = $this->json->unserialize($data);

        $this->dataHandlerDB->updateDatabaseArrayEntityData(OfflineStoreResource::TABLE_NAME, $data);
    }
}