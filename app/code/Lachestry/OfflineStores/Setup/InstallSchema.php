<?php

namespace Lachestry\OfflineStores\Setup;

use Lachestry\OfflineStores\Model\ResourceModel\Employee as EmployeeResource;
use Lachestry\OfflineStores\Model\Employee as EmployeeEntity;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore as OfflineStoreResource;
use Lachestry\OfflineStores\Model\OfflineStore as OfflineStoreEntity;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()
            ->newTable($setup->getTable(OfflineStoreResource::TABLE_NAME))
            ->addColumn(
                OfflineStoreEntity::FIELD_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )->addColumn(
                OfflineStoreEntity::FIELD_TITLE,
                Table::TYPE_TEXT,
                80,
                ['nullable' => false],
                'Title'
            )->addColumn(
                OfflineStoreEntity::FIELD_META_TITLE,
                Table::TYPE_TEXT,
                20,
                ['nullable' => false],
                'Meta-title'
            )->addColumn(
                OfflineStoreEntity::FIELD_SLUG,
                Table::TYPE_TEXT,
                64,
                ['nullable' => false],
                'Slug'
            )->addColumn(
                OfflineStoreEntity::FIELD_IMAGE,
                Table::TYPE_TEXT,
                150,
                ['nullable' => false],
                'Image path'
            )->addColumn(
                OfflineStoreEntity::FIELD_LAT,
                Table::TYPE_FLOAT,
                80,
                ['nullable' => false],
                'Latitude'
            )->addColumn(
                OfflineStoreEntity::FIELD_LNG,
                Table::TYPE_FLOAT,
                80,
                ['nullable' => false],
                'Longitude'
            )->addColumn(
                OfflineStoreEntity::FIELD_EMPLOYEE_QTY,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'default' => 1],
                'The number of employees'
            )->addColumn(
                OfflineStoreEntity::FIELD_AVERAGE_SALARY,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false,  'default' => 30000],
                'Average salary'
            )->addColumn(
                OfflineStoreEntity::FIELD_CITY,
                Table::TYPE_TEXT,
                100,
                ['nullable' => false],
                'Name of the city'
            )->addColumn(
                OfflineStoreEntity::FIELD_CREATED_AT,
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Offline store creation time'
            )->addColumn(
                OfflineStoreEntity::FIELD_UPDATE_AT,
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Offline store modification time'
            )->setComment(
                'Offline store block table'
            );

        $setup->getConnection()->createTable($table);

        $table = $setup->getConnection()
            ->newTable($setup->getTable(EmployeeResource::TABLE_NAME))
            ->addColumn(
                EmployeeEntity::FIELD_ID,
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )->addColumn(
                EmployeeEntity::FIELD_OFFLINE_STORE_ID,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Offline store id'
            )->addColumn(
                EmployeeEntity::FIELD_FIRST_NAME,
                Table::TYPE_TEXT,
                40,
                ['nullable' => false],
                'First name'
            )->addColumn(
                EmployeeEntity::FIELD_LAST_NAME,
                Table::TYPE_TEXT,
                40,
                ['nullable' => false],
                'Surname'
            )->addColumn(
                EmployeeEntity::FIELD_IMAGE,
                Table::TYPE_TEXT,
                150,
                ['nullable' => false],
                'Photo of the employee'
            )->addColumn(
                EmployeeEntity::FIELD_DOB,
                Table::TYPE_DATE,
                null,
                ['nullable' => false],
                'Date of birth'
            )->addColumn(
                EmployeeEntity::FIELD_SALARY,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'default' => 30000],
                'Default salary'
            )->addColumn(
                EmployeeEntity::FIELD_CREATED_AT,
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Employee creation time'
            )->addColumn(
                EmployeeEntity::FIELD_UPDATE_AT,
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'Employee modification time'
            )->addForeignKey(
                $setup->getFkName(
                    EmployeeResource::TABLE_NAME,
                    EmployeeEntity::FIELD_OFFLINE_STORE_ID,
                    OfflineStoreResource::TABLE_NAME,
                    OfflineStoreEntity::FIELD_ID
                ),
                EmployeeEntity::FIELD_OFFLINE_STORE_ID,
                $setup->getTable(EmployeeResource::TABLE_NAME),
                OfflineStoreEntity::FIELD_ID,
                Table::ACTION_CASCADE
            )->setComment(
                'Employee table'
            );

        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}
