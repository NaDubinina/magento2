<?php

namespace Lachestry\OfflineStores\Setup;

use Lachestry\OfflineStores\Model\ResourceModel\Employee as EmployeeResource;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore as OfflineStoreResource;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Module\Dir;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    public const DATA_OFFLINE_STORE_JSON = 'install-store-data.json';
    public const DATA_EMPLOYEE_JSON = 'install-employee-data.json';
    private $dirModule;
    private $json;
    private $fileReader;

    public function __construct(Dir $dirModule, Json $json, File $fileReader)
    {
        $this->fileReader = $fileReader;
        $this->json = $json;
        $this->dirModule = $dirModule;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $modulePath = $this->dirModule->getDir('Lachestry_OfflineStores');
        $dirDataPath = $modulePath . DIRECTORY_SEPARATOR . 'Setup' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR;

        $this->insertData(self::DATA_OFFLINE_STORE_JSON, $dirDataPath, OfflineStoreResource::TABLE_NAME, $setup);
        $this->insertData(self::DATA_EMPLOYEE_JSON, $dirDataPath, EmployeeResource::TABLE_NAME, $setup);
    }

    protected function insertData($jsonName, $pathToJson, $tableName, ModuleDataSetupInterface $setup)
    {
        $data = $this->json->unserialize($this->fileReader->fileGetContents($pathToJson . DIRECTORY_SEPARATOR . $jsonName));
        ($setup->getConnection())->insertMultiple($tableName, $data);
    }
}
