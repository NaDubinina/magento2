<?php

namespace Lachestry\OfflineStores\Setup;

use Lachestry\OfflineStores\Model\ResourceModel\Employee as  EmployeeResource;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore as OfflineStoreResource;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;

class Uninstall implements UninstallInterface
{
    private $tables = [
        OfflineStoreResource::TABLE_NAME,
        EmployeeResource::TABLE_NAME
    ];

    public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $connection = $setup->getConnection();

        foreach ($this->tables as $table) {
            $connection->dropTable($table);
        }

        $setup->endSetup();
    }
}
