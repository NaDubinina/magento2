<?php

namespace Lachestry\OfflineStores\Setup;

use Lachestry\OfflineStores\Model\Employee as EmployeeEntity;
use Lachestry\OfflineStores\Model\ResourceModel\Employee as EmployeeResource;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore as OfflineStoreResource;
use Lachestry\OfflineStores\Model\OfflineStore as OfflineStoreEntity;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->modifySalaryColumn($setup);
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addStoreLinkToOfflineStore($setup);
        }

        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->addColumnIsActive($setup);
            $this->modifyImageColumn($setup);
            $this->modifyUpdatedAtColumn($setup);
        }
    }

    protected function modifySalaryColumn(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->modifyColumn(
            $setup->getTable(OfflineStoreResource::TABLE_NAME),
            OfflineStoreEntity::FIELD_AVERAGE_SALARY,
            [
                'type' => Table::TYPE_INTEGER,
                'nullable' => false,
                'default' => 33000,
                'comment' =>'Average employee salary'
            ]
        );
    }

    protected function addStoreLinkToOfflineStore(SchemaSetupInterface $setup): self
    {
        $originalTable  = OfflineStoreResource::TABLE_NAME;
        $originalField  = OfflineStoreEntity::FIELD_STORE_ID;
        $referenceTable = 'store';
        $referenceField = 'store_id';

        $connection = $setup->getConnection();

        $connection->addColumn(
            $setup->getTable($originalTable),
            $originalField,
            [
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'unsigned' => true,
                'default'  => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                'comment'  => 'Store Id',
                'after'    => OfflineStoreEntity::FIELD_ID,
            ]
        );

        $connection->addForeignKey(
            $setup->getFkName(
                OfflineStoreResource::TABLE_NAME,
                OfflineStoreEntity::FIELD_STORE_ID,
                $referenceTable,
                $referenceField
            ),
            $setup->getTable($originalTable),
            $originalField,
            $setup->getTable($referenceTable),
            $referenceField
        );

        return $this;
    }

    protected function addColumnIsActive($setup)
    {

        $connection = $setup->getConnection();

        $connection->addColumn(
            $setup->getTable(OfflineStoreResource::TABLE_NAME),
            OfflineStoreEntity::FIELD_IS_ACTIVE,
            [
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                'default'  => true,
                'nullable' => false,
                'comment'  => 'Offline store is active',
            ]
        );

        $connection->addColumn(
            $setup->getTable(EmployeeResource::TABLE_NAME),
            EmployeeEntity::FIELD_IS_ACTIVE,
            [
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                'default'  => true,
                'nullable' => false,
                'comment'  => 'Employee is active',
            ]
        );

        return $this;
    }

    protected function modifyImageColumn(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->modifyColumn(
            $setup->getTable(OfflineStoreResource::TABLE_NAME),
            OfflineStoreEntity::FIELD_IMAGE,
            [
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment'  => 'Offline store is active',
            ]
        );

        $setup->getConnection()->modifyColumn(
            $setup->getTable(EmployeeResource::TABLE_NAME),
            EmployeeEntity::FIELD_IMAGE,
            [
                'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment'  => 'Employee is active',
            ]
        );

    }

    protected function modifyUpdatedAtColumn($setup)
    {
        $connection = $setup->getConnection();
        $connection->dropColumn($setup->getTable(OfflineStoreResource::TABLE_NAME), OfflineStoreEntity::FIELD_UPDATE_AT);
        $connection->addColumn(
            $setup->getTable(OfflineStoreResource::TABLE_NAME),
            OfflineStoreEntity::FIELD_UPDATE_AT,
            [
                'type' => Table::TYPE_TIMESTAMP,
                'nullable' => false,
                'default' => Table::TIMESTAMP_INIT_UPDATE,
                'comment' => 'Offline store modification time'
            ]
        );
    }
}
