<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Api;

use Lachestry\OfflineStores\Api\Data\OfflineStoreInterface;
use Lachestry\OfflineStores\Api\Data\OfflineStoreSearchResultInterface;

interface OfflineStoreRepositoryInterface
{
    public function save(\Lachestry\OfflineStores\Model\Data\OfflineStore $offlineStore): OfflineStoreInterface;

    public function getById(int $offlineStoreId): OfflineStoreInterface;

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria): OfflineStoreSearchResultInterface;

    public function delete(\Lachestry\OfflineStores\Model\Data\OfflineStore $offlineStore);

    public function deleteById(int $offlineStoreId);
}
