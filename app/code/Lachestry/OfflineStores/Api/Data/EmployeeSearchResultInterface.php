<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Api\Data;

interface EmployeeSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{

}
