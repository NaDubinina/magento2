<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Api\Data;

interface OfflineStoreSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{

}
