<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Api\Data;

use Lachestry\OfflineStores\Model\Data\Employee;

interface EmployeeInterface
{
    public function getId(): ?int;

    public function setId($id): Employee;

    public function getOfflineStoreId(): ?int;

    public function setOfflineStoreId($offlineStoreId): Employee;

    public function setFirstName($firstNameId): Employee;

    public function getFirstName(): ?string;

    public function setLastName($lastNameId): Employee;

    public function getLastName(): ?string;

    public function getImage(): ?string;

    public function setImage($image): Employee;

    public function getSalary(): ?string;

    public function setSalary($image): Employee;

    public function getDob(): ?string;

    public function setDob($image): Employee;

    public function getCreatedAt();

    public function setCreatedAt($createdAt): Employee;

    public function getUpdatedAt();

    public function setUpdatedAt($updatedAt): Employee;

    public function setData($key, $value);

    public function __toArray();
}
