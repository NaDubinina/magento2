<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Api\Data;

use Lachestry\OfflineStores\Model\Data\OfflineStore;
use Magento\Framework\Api\AbstractSimpleObject;

interface OfflineStoreInterface
{
    public function getId(): ?int;

    public function setId($id): OfflineStore;

    public function getStoreId(): ?int;

    public function setStoreId($storeId): OfflineStore;

    public function getTitle(): string;

    public function setTitle($title): OfflineStore;

    public function getMetaTitle(): string;

    public function setMetaTitle($metaTitle): OfflineStore;

    public function getSlug(): string;

    public function setSlug($slug): OfflineStore;

    public function getImage(): string;

    public function setImage($image): OfflineStore;

    public function getEmployeeQty(): ?int;

    public function setEmployeeQty($employeeQty): OfflineStore;

    public function getAverageSalary(): ?int;

    public function setAverageSalary($averageSalary): OfflineStore;

    public function getCity(): string;

    public function setCity($city): OfflineStore;

    public function getLat(): ?float;

    public function setLat($lat): OfflineStore;

    public function getLng(): ?float;

    public function setLng($lng): OfflineStore;

    public function getCreatedAt();

    public function setCreatedAt($createdAt): OfflineStore;

    public function getUpdatedAt();

    public function setUpdatedAt($updatedAt): OfflineStore;

    public function setData($key, $value);

    public function __toArray();
}
