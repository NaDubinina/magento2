<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Api;

use Lachestry\OfflineStores\Api\Data\EmployeeInterface;
use Lachestry\OfflineStores\Api\Data\EmployeeSearchResultInterface;
use Lachestry\OfflineStores\Model\Data\Employee;

interface EmployeeRepositoryInterface
{
    public function save(\Lachestry\OfflineStores\Model\Data\Employee $employee): EmployeeInterface;

    public function getById(int $offlineStoreId): EmployeeInterface;

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria): EmployeeSearchResultInterface;

    public function delete(\Lachestry\OfflineStores\Model\Data\Employee $employee);

    public function deleteById(int $employeeId);
}
