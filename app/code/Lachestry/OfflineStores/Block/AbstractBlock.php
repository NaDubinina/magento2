<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Block;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\BlockInterface;
use Magento\Framework\View\Element\Template;

/**
 * @method string  getOfflineStoreTitle()
 * @method self    setOfflineStoreTitle(string $storeTitle)
 */
class AbstractBlock extends Template implements BlockInterface
{
    public  function getStaticImage(string $staticImagePath): string
    {
        $pathToStaticFolder = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_STATIC]);
        return $this->getPathToImage($pathToStaticFolder, $staticImagePath);
    }

    public function getMediaImage(string $mediaImagePath): string
    {
        $pathToMediaFolder = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
        return $this->getPathToImage($pathToMediaFolder, $mediaImagePath);
    }

    private function getPathToImage($folderPath, $imgPath)
    {
         return trim($folderPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . trim($imgPath, DIRECTORY_SEPARATOR);
    }
}
