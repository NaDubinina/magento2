<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Block\Adminhtml\Employee;

use Lachestry\OfflineStores\Model\Employee;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected $coreRegistry = null;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }


    protected function _construct()
    {
        $this->_objectId = Employee::FIELD_ID;
        $this->_controller = 'adminhtml_employee';
        $this->_blockGroup = 'Lachestry_OfflineStores';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Employee'));
        $this->buttonList->update('delete', 'label', __('Delete Employee'));

        $this->buttonList->add(
            'save_and_continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form']],
                ]
            ],
            5
        );
    }
}
