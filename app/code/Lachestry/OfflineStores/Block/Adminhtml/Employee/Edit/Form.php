<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Block\Adminhtml\Employee\Edit;

use Lachestry\OfflineStores\Model\Employee;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\Collection as OfflineStoreCollection;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected $yesNoConfig;
    protected $collectionOfflineStore;

    public function __construct(
        \Magento\Backend\Block\Template\Context   $context,
        \Magento\Framework\Registry               $registry,
        \Magento\Framework\Data\FormFactory       $formFactory,
        \Magento\Config\Model\Config\Source\Yesno $yesNoConfig,
        OfflineStoreCollection                    $collectionOfflineStore,
        array                                     $data = []
   ) {
        parent::__construct($context, $registry, $formFactory, $data);

        $this->collectionOfflineStore = $collectionOfflineStore;
        $this->yesNoConfig = $yesNoConfig;
    }

    protected function _construct()
    {
        parent::_construct();

        $this->setId('EmployeeForm');
        $this->setTitle(__('Employee'));
        $this->setUseContainer(true);
    }


    protected function _prepareForm()
    {
        $offlineStoreModel = $this->getEmployeeModel();
        $form = $this->createForm();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Tax Rule Information')]);

        if ($offlineStoreModel->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id',]);
        }

        $fieldset->addField(
            Employee::FIELD_FIRST_NAME,
            'text',
            [
                'name' => Employee::FIELD_FIRST_NAME,
                'label' => __('First name'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            Employee::FIELD_LAST_NAME,
            'text',
            [
                'name' => Employee::FIELD_LAST_NAME,
                'label' => __('Last name'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            Employee::FIELD_IS_ACTIVE,
            'select',
            [
                'name' => Employee::FIELD_IS_ACTIVE,
                'label' => __('Is employee active'),
                'class' => 'required-entry',
                'required' => true,
                'options' => $this->yesNoConfig->toArray(),
            ]
        );

        $fieldset->addField(
            Employee::FIELD_IMAGE,
            'image',
            [
                'name' => Employee::FIELD_IMAGE,
                'label' => __('Image'),
                'class' => 'required-entry required-file',
                'required' => false
            ]
        );

        $fieldset->addField(
            Employee::FIELD_SALARY,
            'text',
            [
                'name' => Employee::FIELD_SALARY,
                'label' => __('Salary'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            Employee::FIELD_DOB,
            'date',
            [
                'name' => Employee::FIELD_DOB,
                'label' => __('Date of Birth'),
                'class' => 'required-entry',
                'date_format' => 'yyyy-MM-dd',
                'required' => true
            ]
        );

        $fieldset->addField(
            Employee::FIELD_OFFLINE_STORE_ID,
            'select',
            [
                'name' => Employee::FIELD_OFFLINE_STORE_ID,
                'label' => __('Offline Store'),
                'options' => $this->getOfflineStoreIds(),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $form->setAction($this->getUrl('offlinestores/employee/save'));
        $form->setValues($offlineStoreModel->getData());
        $form->setUseContainer($this->getUseContainer());
        $this->setForm($form);

        return parent::_prepareForm();
    }


    protected function createForm(): \Magento\Framework\Data\Form
    {
        return $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ]
            ]
        );
    }

    protected function getOfflineStoreIds(): array
    {
        $storeIds = [];
        $stores = $this->collectionOfflineStore->getItems();

        foreach ($stores as $store) {
            $storeIds[$store->getId()] = $store->getTitle();
        }
        return $storeIds;
    }

    protected function getEmployeeModel(): Employee
    {
        return $this->_coreRegistry->registry('offlineStores_employee');
    }
}
