<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Block\Adminhtml\Employee;

use Lachestry\OfflineStores\Block\Adminhtml\Employee\Renderer\LinkRenderer;
use Lachestry\OfflineStores\Block\Adminhtml\Renderer\ImageRenderer;
use Lachestry\OfflineStores\Model\ResourceModel\Employee\Collection;
use Lachestry\OfflineStores\Model\ResourceModel\Employee\CollectionFactory;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $collectionFactory;

    protected $yesNoConfig;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        CollectionFactory $collectionFactory,
        \Magento\Config\Model\Config\Source\Yesno $yesNoConfig,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->yesNoConfig = $yesNoConfig;

        $this->setId('Lachestry_OfflineStores::employees');
        $this->setDefaultSort('identifier');
        $this->setDefaultDir('ASC');

        parent::__construct($context, $backendHelper, $data);
    }

    /** @var \Lachestry\OfflineStores\Model\Employee $employee */
    public function getRowClass($employee)
    {
        return $employee->getIsActive() ? 'active-offlinestores-entity' : 'inactive-offlinestores-entity';
    }

    protected function _prepareCollection(): \Magento\Backend\Block\Widget\Grid
    {
        $collection = $this->collectionFactory->create();
        /* @var Collection $collection */
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns(): \Magento\Backend\Block\Widget\Grid\Extended
    {
        $this->addColumn(
            'salary',
            [
                'header'   => __('Salary'),
                'index'    => 'salary',
                'type'     => 'text',
                'sortable' => true
            ]
        );

        $this->addColumn(
            'last_name',
            [
                'header'   => __('Last name'),
                'index'    => 'last_name',
                'type'     => 'text',
                'sortable' => true
            ]
        );

        $this->addColumn(
            'first_name',
            [
                'header'   => __('First name'),
                'index'    => 'first_name',
                'type'     => 'text',
                'sortable' => true
            ]
        );

        $this->addColumn(
            'is_active',
            [
                'header'  => __('Is active'),
                'index'   => 'is_active',
                'type'    => 'options',
                'options' => $this->yesNoConfig->toArray(),
            ]
        );

        $this->addColumn(
            'href',
            [
                'header'   => __('Offline Store Link'),
                'type'     => 'text',
                'sortable' => false,
                'renderer' => LinkRenderer::class,
            ]
        );

        $this->addColumn(
            'image',
            [
                'header'   => __('Image'),
                'index'    => 'image',
                'type'     => 'image',
                'sortable' => false,
                'renderer' => ImageRenderer::class,
            ]
        );

        $this->addColumn(
            'created_at',
            [
                'header' => __('Created'),
                'index'  => 'created_at',
                'type'   => 'datetime',
                'header_css_class' => 'col-date',
                'column_css_class' => 'col-date'
            ]
        );

        $this->addColumn(
            'updated_at',
            [
                'header' => __('Modified'),
                'index'  => 'updated_at',
                'type'   => 'datetime',
                'header_css_class' => 'col-date',
                'column_css_class' => 'col-date'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * @param \Magento\Framework\DataObject $employeeEntity
     */
    public function getRowUrl($employeeEntity): string
    {
        return $this->getUrl('*/*/edit', ['employee_id' => $employeeEntity->getId()]);
    }
}
