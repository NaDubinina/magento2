<?php

namespace Lachestry\OfflineStores\Block\Adminhtml\Employee\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class LinkRenderer extends AbstractRenderer
{
    public function render(DataObject $entityData)
    {
        $entityLink = $this->_urlBuilder->getRouteUrl('employee', ['id' =>  $entityData->getId()]);
        $entityLink = str_replace('/index.php/admin', '', $entityLink);
        
        return "<a href='{$entityLink}'>{$entityLink}</a>";
    }
}