<?php

namespace Lachestry\OfflineStores\Block\Adminhtml\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;
use Magento\Framework\UrlInterface;

class ImageRenderer extends AbstractRenderer
{
    public function render(DataObject $entityData)
    {
        if ($entityData->getImage()) {
            $imageUrl = $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
            $imageUrl .= $entityData->getImage();

            return '<img width="40" height="40" src="' . $imageUrl . '">';
        }

        return '';
    }
}