<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Block\Adminhtml;

class Employee extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_employee';
        $this->_blockGroup = 'Lachestry_OfflineStores';
        $this->_headerText = __('Employee');

        parent::_construct();

        $this->buttonList->update('add', 'label', __('Add New Employee'));

    }
}
