<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Block\Adminhtml\OfflineStore;

use Lachestry\OfflineStores\Block\Adminhtml\OfflineStore\Renderer\LinkRenderer;
use Lachestry\OfflineStores\Block\Adminhtml\Renderer\ImageRenderer;
use Lachestry\OfflineStores\Controller\Adminhtml\OfflineStore\MassChangeIsActive;
use Lachestry\OfflineStores\Model\OfflineStore;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\Collection;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\CollectionFactory;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $collectionFactory;

    protected $yesNoConfig;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        CollectionFactory $collectionFactory,
        \Magento\Config\Model\Config\Source\Yesno $yesNoConfig,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->yesNoConfig = $yesNoConfig;

        $this->setId('Lachestry_OfflineStores::offlinestores_offlinestore');
        $this->setDefaultSort('identifier');
        $this->setDefaultDir('ASC');

        parent::__construct($context, $backendHelper, $data);
    }



    /** @var \Lachestry\OfflineStores\Model\OfflineStore $offlineStore */
    public function getRowClass($offlineStore)
    {
        return $offlineStore->getIsActive() ? 'active-offlinestores-entity' : 'inactive-offlinestores-entity';
    }


    protected function _prepareCollection(): \Magento\Backend\Block\Widget\Grid
    {
        $collection = $this->collectionFactory->create();
        /* @var Collection $collection */
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }


    protected function _prepareColumns(): \Magento\Backend\Block\Widget\Grid\Extended
    {
        $this->addColumn(
            'meta_title',
            [
                'header'   => __('Meta title'),
                'index'    => 'meta_title',
                'type'     => 'text',
                'sortable' => true
            ]
        );

        $this->addColumn(
            'title',
            [
                'header'   => __('Title'),
                'index'    => 'title',
                'type'     => 'text',
                'sortable' => true
            ]
        );

        $this->addColumn(
            'is_active',
            [
                'header'  => __('Is active'),
                'index'   => 'is_active',
                'type'    => 'options',
                'options' => $this->yesNoConfig->toArray(),
            ]
        );

        $this->addColumn(
            'slug',
            [
                'header'   => __('URL Key'),
                'index'    => 'slug',
                'type'     => 'text',
                'sortable' => true
            ]
        );

        $this->addColumn(
            'href',
            [
                'header'   => __('Offline Store Link'),
                'type'     => 'text',
                'sortable' => false,
                'renderer' => LinkRenderer::class,
            ]
        );

        $this->addColumn(
            'image',
            [
                'header'   => __('Image'),
                'index'    => 'image',
                'type'     => 'image',
                'sortable' => false,
                'renderer' => ImageRenderer::class
            ]
        );

        if (!$this->_storeManager->isSingleStoreMode()) {
            $this->addColumn(
                'store_id',
                [
                    'header'     => __('Store View'),
                    'index'      => 'store_id',
                    'type'       => 'store',
                    'store_all'  => true,
                    'store_view' => true,
                    'sortable'   => false,
                ]
            );
        }

        $this->addColumn(
            'created_at',
            [
                'header' => __('Created'),
                'index'  => 'created_at',
                'type'   => 'datetime',
                'header_css_class' => 'col-date',
                'column_css_class' => 'col-date'
            ]
        );

        $this->addColumn(
            'updated_at',
            [
                'header' => __('Modified'),
                'index'  => 'updated_at',
                'type'   => 'datetime',
                'header_css_class' => 'col-date',
                'column_css_class' => 'col-date'
            ]
        );

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField(OfflineStore::FIELD_ID);
        $this->getMassactionBlock()->setFormFieldName('selected');
        $this->getMassactionBlock()->addItem(
            'OfflineStores_MassActive',
            [
                'label' => __('Mass Active'),
                'url' => $this->getUrl('*/*/massChangeIsActive'),
                'additional' => [
                    'is_active' => [
                        'name' => 'is_active',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Is Active'),
                        'values' => [true => 'Yes', false => 'No'],
                    ],
                ]
            ]
        );

        return parent::_prepareMassaction();
    }

    /**
     * @param \Magento\Framework\DataObject $offlineStoreEntity
     */
    public function getRowUrl($offlineStoreEntity): string
    {
        return $this->getUrl('*/*/edit', ['offlinestore_id' => $offlineStoreEntity->getId()]);
    }
}
