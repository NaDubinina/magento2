<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Block\Adminhtml\OfflineStore;

use Lachestry\OfflineStores\Model\OfflineStore;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected $coreRegistry = null;

    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->coreRegistry = $registry;
    }

    protected function _construct()
    {
        $this->_objectId = OfflineStore::FIELD_ID;
        $this->_controller = 'adminhtml_offlineStore';
        $this->_blockGroup = 'Lachestry_OfflineStores';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Offline Store'));
        $this->buttonList->update('delete', 'label', __('Delete Offline Store'));

        $this->buttonList->add(
            'save_and_continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form']],
                ]
            ],
            5
        );
    }
}
