<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Block\Adminhtml\OfflineStore\Edit;

use Lachestry\OfflineStores\Model\OfflineStore;
use Magento\Store\Model\ResourceModel\Store\Collection as Store;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected $yesNoConfig;
    protected $storeCollection;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Config\Model\Config\Source\Yesno $yesNoConfig,
        Store $storeCollection,
        array $data = []
   ) {
        parent::__construct($context, $registry, $formFactory, $data);

        $this->storeCollection = $storeCollection;
        $this->yesNoConfig = $yesNoConfig;
    }

    protected function _construct()
    {
        parent::_construct();

        $this->setId('OfflineStoreForm');
        $this->setTitle(__('Offline Store'));
        $this->setUseContainer(true);
    }


    protected function _prepareForm()
    {
        $offlineStoreModel = $this->getOfflineStoreModel();
        $form = $this->createForm();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Tax Rule Information')]);

        if ($offlineStoreModel->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id',]);
        }

        $fieldset->addField(
            OfflineStore::FIELD_STORE_ID,
            'select',
            [
                'name' => OfflineStore::FIELD_STORE_ID,
                'label' => __('Offline Store'),
                'options' => $this->getOfflineStoreIds(),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_TITLE,
            'text',
            [
                'name' => OfflineStore::FIELD_TITLE,
                'label' => __('Title'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_META_TITLE,
            'text',
            [
                'name' => OfflineStore::FIELD_META_TITLE,
                'label' => __('Meta title'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_IS_ACTIVE,
            'select',
            [
                'name' => OfflineStore::FIELD_IS_ACTIVE,
                'label' => __('Is employee active'),
                'class' => 'required-entry',
                'required' => true,
                'options' => $this->yesNoConfig->toArray(),
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_IMAGE,
            'image',
            [
                'name' => OfflineStore::FIELD_IMAGE,
                'label' => __('Image'),
                'class' => 'required-entry required-file',
                'required' => false
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_LNG,
            'text',
            [
                'name' => OfflineStore::FIELD_LNG,
                'label' => __('lng'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_LAT,
            'text',
            [
                'name' => OfflineStore::FIELD_LAT,
                'label' => __('lat'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_CITY,
            'text',
            [
                'name' => OfflineStore::FIELD_CITY,
                'label' => __('City'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_AVERAGE_SALARY,
            'text',
            [
                'name' => OfflineStore::FIELD_AVERAGE_SALARY,
                'label' => __('Average Salary'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_EMPLOYEE_QTY,
            'text',
            [
                'name' => OfflineStore::FIELD_EMPLOYEE_QTY,
                'label' => __('Employee qty'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $fieldset->addField(
            OfflineStore::FIELD_SLUG,
            'text',
            [
                'name' => OfflineStore::FIELD_SLUG,
                'label' => __('Slug'),
                'class' => 'required-entry',
                'required' => true
            ]
        );

        $form->setAction($this->getUrl('offlinestores/offlinestore/save'));
        $form->setValues($offlineStoreModel->getData());
        $form->setUseContainer($this->getUseContainer());
        $this->setForm($form);

        return parent::_prepareForm();
    }


    protected function createForm(): \Magento\Framework\Data\Form
    {
        return $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ]
            ]
        );
    }

    protected function getOfflineStoreIds(): array
    {
        $storeIds = [];
        $stores = $this->storeCollection->getItems();

        foreach ($stores as $store) {
            $storeIds[$store->getStoreId()] = $store->getName();
        }
        return $storeIds;
    }

    protected function getOfflineStoreModel(): OfflineStore
    {
        return $this->_coreRegistry->registry('offlineStores_offlinestore');
    }
}
