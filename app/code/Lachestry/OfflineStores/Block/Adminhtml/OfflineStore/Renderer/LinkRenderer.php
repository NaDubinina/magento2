<?php

namespace Lachestry\OfflineStores\Block\Adminhtml\OfflineStore\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class LinkRenderer extends AbstractRenderer
{
    public function render(DataObject $entityData)
    {
        $entityLink = $this->_urlBuilder->getRouteUrl('offlinestores') . $entityData->getSlug();
        $entityLink = str_replace('/index.php/admin', '', $entityLink);

        return "<a href='{$entityLink}'>{$entityLink}</a>";
    }
}