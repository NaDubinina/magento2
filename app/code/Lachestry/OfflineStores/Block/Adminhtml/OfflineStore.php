<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Block\Adminhtml;

class OfflineStore extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_offlineStore';
        $this->_blockGroup = 'Lachestry_OfflineStores';
        $this->_headerText = __('Offline Stores');

        parent::_construct();

        $this->buttonList->update('add', 'label', __('Add New Offline Store'));

    }
}
