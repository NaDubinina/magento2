<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Block\OfflineStore;

use Lachestry\OfflineStores\Block\AbstractBlock;

class OfflineStoresList extends AbstractBlock
{
    public const BLOCK_NAME = 'lachestry.offlinestore.list';
}
