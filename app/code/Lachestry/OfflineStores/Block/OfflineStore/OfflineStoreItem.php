<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Block\OfflineStore;

use Lachestry\OfflineStores\Block\AbstractBlock;
use Lachestry\OfflineStores\Model\OfflineStore;

/**
 * @method OfflineStore  getOfflineStore()
 * @method self    setOfflineStore(OfflineStore $offlineStore)
 */
class OfflineStoreItem extends AbstractBlock
{
    public const BLOCK_NAME = 'lachestry.offlinestore.list.item';
}
