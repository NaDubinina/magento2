require(['jquery'], function ($) {
    $(document).ready(function () {
        $('.active-offlinestores-entity td').mouseenter(function (event) {
            const $target = $(event.target);
            $target.css('font-weight', '700');
        });
        $('.active-offlinestores-entity td').mouseleave(function (event) {
            const $target = $(event.target);
            $target.css('font-weight', '400');
        });
        $('.inactive-offlinestores-entity td').mouseenter(function (event) {
            const $target = $(event.target);
            $target.css('font-weight', '700');
        });
        $('.inactive-offlinestores-entity td').mouseleave(function (event) {
            const $target = $(event.target);
            $target.css('font-weight', '400');
        });
    });
});

