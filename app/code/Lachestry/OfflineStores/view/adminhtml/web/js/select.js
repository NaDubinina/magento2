define([
    'underscore',
    'Magento_Ui/js/grid/columns/select'
], function (_, Column) {
    'use strict';

    return Column.extend({
        defaults: {
            bodyTmpl: 'Lachestry_OfflineStores/grid/cells/is-active-color'
        },
        getIsActiveClass: function (row) {
            console.log(row.is_active);
            if (row.is_active == true) {
                return 'active-offlinestores-entity';
            }else {
                return 'inactive-offlinestores-entity';
            }
        }
    });
});