<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\Plugin;

use Lachestry\OfflineStores\Api\OfflineStoreRepositoryInterface;
use Lachestry\OfflineStores\Model\Data\OfflineStore;

class OfflineStoreGetByIdPluginB
{
    public function beforeGetById(
        OfflineStoreRepositoryInterface $subject,
        int $id
   ) {
        echo('B before getById <br>');

        return $id;
    }

    public function aroundGetById(
        OfflineStoreRepositoryInterface $subject,
        callable $proceed,
        $id
   ) {
        echo('B around getById 1 <br>');

        $offlineStore = $proceed($id);

        echo('B around getById 2 <br>');

        return $offlineStore;
    }

    public function afterGetById(
        OfflineStoreRepositoryInterface $subject,
        OfflineStore $offlineStore
    )
    {
        echo('B after getById <br>');
        return $offlineStore;
    }
}
