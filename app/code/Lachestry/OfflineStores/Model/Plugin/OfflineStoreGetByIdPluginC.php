<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\Plugin;

use Lachestry\OfflineStores\Api\OfflineStoreRepositoryInterface;
use Lachestry\OfflineStores\Model\Data\OfflineStore;

class OfflineStoreGetByIdPluginC
{
    public function beforeGetById(
        OfflineStoreRepositoryInterface $subject,
        int $id
    )
    {
        echo('C before getById <br>');

        return $id;
    }

    public function aroundGetById(
        OfflineStoreRepositoryInterface $subject,
        callable $proceed,
        $id
    )
    {
        echo('C around getById 1 <br>');

        $offlineStore = $proceed($id);

        echo('C around getById 2 <br>');

        return $offlineStore;
    }

    public function afterGetById(
        OfflineStoreRepositoryInterface $subject,
        OfflineStore $offlineStore
    )
    {
        echo('C after getById <br>');
        return $offlineStore;
    }
}
