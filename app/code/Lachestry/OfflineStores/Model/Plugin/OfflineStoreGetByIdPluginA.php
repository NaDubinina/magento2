<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\Plugin;

use Lachestry\OfflineStores\Api\OfflineStoreRepositoryInterface;
use Lachestry\OfflineStores\Model\Data\OfflineStore;

class OfflineStoreGetByIdPluginA
{
    public function beforeGetById(
        OfflineStoreRepositoryInterface $subject,
        int $id
   ) {
        echo('A1 before getById <br>');

        return $id;
    }

    public function aroundGetById(
        OfflineStoreRepositoryInterface $subject,
        callable $proceed,
        $id
   ) {
        echo('A1 around getById 1 <br>');

        $offlineStore = $proceed($id);

        echo('A1 around getById 2<br>');

        return $offlineStore;
    }

    public function afterGetById(
        OfflineStoreRepositoryInterface $subject,
        OfflineStore $offlineStore
   ) {
        echo('A1 after getById <br>');
        return $offlineStore;
    }
}
