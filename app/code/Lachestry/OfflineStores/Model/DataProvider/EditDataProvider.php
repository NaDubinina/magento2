<?php

namespace Lachestry\OfflineStores\Model\DataProvider;

use Lachestry\OfflineStores\Model\OfflineStore;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\Grid\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

class EditDataProvider extends AbstractDataProvider
{
    private $loadedData;

    private $mediaDirectory;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \Lachestry\OfflineStores\Model\ResourceModel\OfflineStore $resource,
        \Lachestry\OfflineStores\Model\OfflineStoreFactory $offlineStoreFactory,
        RequestInterface $request,
        Filesystem $filesystem,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->request = $request;
        $this->resource = $resource;
        $this->storeManager = $storeManager;
        $this->offlineStoreFactory = $offlineStoreFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->mediaDirectory = $filesystem->getDirectoryRead(DirectoryList::MEDIA);
    }

    public function getData(): ?array
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $offlineStore = $this->getCurrentOfflineStore();

        $offlineStoreData = $offlineStore->getData();

        if (!$offlineStoreData) {
            return null;
        }

        $image = $offlineStoreData[OfflineStore::FIELD_IMAGE];

        if (!$image) {
            $this->loadedData[$offlineStore->getId()] = $offlineStoreData;
            return $this->loadedData;
        }

        $baseUrl = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);

        $fullImagePath = $this->mediaDirectory->getAbsolutePath($image);
        $imageUrl = $baseUrl . $image;
        $stat = $this->mediaDirectory->stat($fullImagePath);

        $offlineStoreData[OfflineStore::FIELD_IMAGE] = null;
        $offlineStoreData[OfflineStore::FIELD_IMAGE][0]['url'] = $imageUrl;
        $offlineStoreData[OfflineStore::FIELD_IMAGE][0]['name'] = $image;
        $offlineStoreData[OfflineStore::FIELD_IMAGE][0]['size'] = $stat['size'];

        $this->loadedData[$offlineStore->getId()] = $offlineStoreData;

        return $this->loadedData;
    }

    private function getCurrentOfflineStore(): OfflineStore
    {
        $offlineStoreId = $this->getOfflineStoreId();
        $offlineStore = $this->offlineStoreFactory->create();
        if (!$offlineStoreId) {
            return $offlineStore;
        }

        $this->resource->load($offlineStore, $offlineStoreId);

        return $offlineStore;
    }

    private function getOfflineStoreId()
    {
        return $this->request->getParam(OfflineStore::FIELD_ID);
    }
}
