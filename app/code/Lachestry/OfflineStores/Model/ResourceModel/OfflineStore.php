<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class OfflineStore extends AbstractDb
{
    public const TABLE_NAME = 'lachestry_offlinestores_offlinestore';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, \Lachestry\OfflineStores\Model\OfflineStore::FIELD_ID);
    }
}
