<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\ResourceModel\OfflineStore;

use Lachestry\OfflineStores\Model\OfflineStore;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * @method OfflineStore[] getItems()
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            OfflineStore::class,
            \Lachestry\OfflineStores\Model\ResourceModel\OfflineStore::class
        );
    }
}
