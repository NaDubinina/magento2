<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\Grid;

use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\Collection as OfflineStoreCollection;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore;
use Magento\Framework\Api\SearchCriteriaInterface;

class Collection extends OfflineStoreCollection implements SearchResultInterface
{
    protected $aggregations;

    protected function _construct()
    {
        $this->_init(Document::class, OfflineStore::class);
    }

    public function getAggregations()
    {
        return $this->aggregations;
    }

    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }

//    public function getAllIds($limit = null, $offset = null)
//    {
//        return $this->getConnection()
//            ->fetchCol(
//                $this->_getAllIdsSelect($limit, $offset),
//                $this->_bindParams
//            );
//    }

    public function getSearchCriteria()
    {
        return null;
    }

    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    public function getTotalCount()
    {
        return $this->getSize();
    }

    public function setTotalCount($totalCount)
    {
        return $this;
    }

    public function setItems(array $items = null)
    {
        return $this;
    }
}
