<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\ResourceModel\Employee;

use Lachestry\OfflineStores\Model\Employee;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * @method Employee[] getItems()
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            Employee::class,
            \Lachestry\OfflineStores\Model\ResourceModel\Employee::class
        );
    }

}
