<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model;

use Magento\Framework\Model\AbstractModel;

class Employee extends AbstractModel
{
    public const FIELD_ID = 'id';
    public const FIELD_OFFLINE_STORE_ID = 'offline_store_id';
    public const FIELD_FIRST_NAME = 'first_name';
    public const FIELD_LAST_NAME = 'last_name';
    public const FIELD_IMAGE = 'image';
    public const FIELD_DOB = 'dob';
    public const FIELD_SALARY = 'salary';
    public const FIELD_CREATED_AT = 'created_at';
    public const FIELD_UPDATE_AT = 'updated_at';
    public const FIELD_IS_ACTIVE = 'is_active';


    protected function _construct()
    {
        $this->_init(\Lachestry\OfflineStores\Model\ResourceModel\Employee::class);
    }
}
