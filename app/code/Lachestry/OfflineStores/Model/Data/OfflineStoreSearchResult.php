<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\Data;

use Lachestry\OfflineStores\Api\Data\OfflineStoreSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

class OfflineStoreSearchResult implements OfflineStoreSearchResultInterface
{
    const KEY_ITEMS = 'items';
    const KEY_SEARCH_CRITERIA = 'search_criteria';
    const KEY_TOTAL_COUNT = 'total_count';
    private $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    private function getData($key)
    {
        return $this->data[$key] ?? null;
    }

    private function setData($key, $value)
    {
        $this->data[$key] = $value;
        return $this;
    }

    public function getItems()
    {
        return $this->getData(self::KEY_ITEMS) === null ? [] : $this->getData(self::KEY_ITEMS);
    }

    public function setItems(array $items)
    {
        return $this->setData(self::KEY_ITEMS, $items);
    }

    public function getSearchCriteria()
    {
        return $this->getData(self::KEY_SEARCH_CRITERIA);
    }

    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria)
    {
        return $this->setData(self::KEY_SEARCH_CRITERIA, $searchCriteria);
    }

    public function getTotalCount()
    {
        return $this->getData(self::KEY_TOTAL_COUNT);
    }

    public function setTotalCount($count)
    {
        return $this->setData(self::KEY_TOTAL_COUNT, $count);
    }
}
