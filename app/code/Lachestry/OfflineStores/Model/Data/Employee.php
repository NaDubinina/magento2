<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\Data;

use Lachestry\OfflineStores\Api\Data\EmployeeInterface;
use Magento\Framework\Api\AbstractSimpleObject;
use Lachestry\OfflineStores\Model\Employee as EmployeeEntity;

class Employee extends AbstractSimpleObject implements EmployeeInterface
{
    public function getId(): ?int
    {
        $id = $this->_get(EmployeeEntity::FIELD_ID);
        return $id ? (int) $id : null;
    }

    public function setId($id): self
    {
        $this->setData(EmployeeEntity::FIELD_ID, (int) $id);
        return $this;
    }

    public function getIsActive(): ?bool
    {
        $isActive = $this->_get(EmployeeEntity::FIELD_IS_ACTIVE);
        return $isActive ? (bool) $isActive : null;
    }

    public function setIsActive($isActive): self
    {
        $this->setData(EmployeeEntity::FIELD_IS_ACTIVE, (bool) $isActive);
        return $this;
    }

    public function getOfflineStoreId(): ?int
    {
        $offlineStoreId = $this->_get(EmployeeEntity::FIELD_OFFLINE_STORE_ID);
        return $offlineStoreId ? (int) $offlineStoreId : null;
    }

    public function setOfflineStoreId($offlineStoreId): self
    {
        $this->setData(EmployeeEntity::FIELD_OFFLINE_STORE_ID, (int) $offlineStoreId);
        return $this;
    }

    public function setFirstName($firstName): self
    {
        $this->setData(EmployeeEntity::FIELD_FIRST_NAME, $firstName);
        return $this;
    }

    public function getFirstName(): ?string
    {
        $firstNameId = $this->_get(EmployeeEntity::FIELD_FIRST_NAME);
        return $firstNameId ?? null;
    }

    public function setLastName($lastName): self
    {
        $this->setData(EmployeeEntity::FIELD_LAST_NAME, $lastName);
        return $this;
    }

    public function getLastName(): ?string
    {
        $lastNameId = $this->_get(EmployeeEntity::FIELD_LAST_NAME);
        return $lastNameId ?? null;
    }

    public function getImage(): ?string
    {
        return $this->_get(EmployeeEntity::FIELD_IMAGE) ?? null;
    }

    public function setImage($image): self
    {
        $this->setData(EmployeeEntity::FIELD_IMAGE, $image);
        return $this;
    }

    public function getSalary(): ?string
    {
        return $this->_get(EmployeeEntity::FIELD_SALARY) ?? null;
    }

    public function setSalary($salary): self
    {
        $this->setData(EmployeeEntity::FIELD_SALARY, $salary);
        return $this;
    }

    public function getDob(): ?string
    {
        return $this->_get(EmployeeEntity::FIELD_DOB) ?? null;
    }

    public function setDob($dob): self
    {
        $this->setData(EmployeeEntity::FIELD_DOB, $dob);
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->_get(EmployeeEntity::FIELD_CREATED_AT);
    }

    public function setCreatedAt($createdAt): self
    {
        $this->setData(EmployeeEntity::FIELD_CREATED_AT, $createdAt);
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->_get(EmployeeEntity::FIELD_UPDATE_AT);
    }

    public function setUpdatedAt($updatedAt = null): self
    {
        $this->setData(EmployeeEntity::FIELD_UPDATE_AT, $updatedAt);
        return $this;
    }
}
