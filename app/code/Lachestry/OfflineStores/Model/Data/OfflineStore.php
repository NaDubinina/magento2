<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\Data;

use Lachestry\OfflineStores\Api\Data\OfflineStoreInterface;
use Magento\Framework\Api\AbstractSimpleObject;
use Lachestry\OfflineStores\Model\OfflineStore as OfflineStoreEntity;

class OfflineStore extends AbstractSimpleObject implements OfflineStoreInterface
{
    public function getId(): ?int
    {
        $id = $this->_get(OfflineStoreEntity::FIELD_ID);
        return $id ? (int) $id : null;
    }

    public function setId($id): self
    {
        $this->setData(OfflineStoreEntity::FIELD_ID, (int) $id);
        return $this;
    }

    public function getStoreId(): ?int
    {
        $storeId = $this->_get(OfflineStoreEntity::FIELD_STORE_ID);
        return $storeId ? (int) $storeId : null;
    }

    public function setStoreId($storeId): self
    {
        $this->setData(OfflineStoreEntity::FIELD_STORE_ID, (int) $storeId);
        return $this;
    }

    public function getIsActive(): ?bool
    {
        $isActive = $this->_get(OfflineStoreEntity::FIELD_IS_ACTIVE);
        return $isActive ? (bool) $isActive : null;
    }

    public function setIsActive($isActive): self
    {
        $this->setData(OfflineStoreEntity::FIELD_IS_ACTIVE, (bool) $isActive);
        return $this;
    }

    public function getTitle(): string
    {
        return $this->_get(OfflineStoreEntity::FIELD_TITLE);
    }

    public function setTitle($title): self
    {
        $this->setData(OfflineStoreEntity::FIELD_TITLE, $title);
        return $this;
    }

    public function getMetaTitle(): string
    {
        return $this->_get(OfflineStoreEntity::FIELD_META_TITLE);
    }

    public function setMetaTitle($metaTitle): self
    {
        $this->setData(OfflineStoreEntity::FIELD_META_TITLE, $metaTitle);
        return $this;
    }

    public function getSlug(): string
    {
        return $this->_get(OfflineStoreEntity::FIELD_SLUG);
    }

    public function setSlug($slug): self
    {
        $this->setData(OfflineStoreEntity::FIELD_SLUG, $slug);
        return $this;
    }

    public function getImage(): string
    {
        return $this->_get(OfflineStoreEntity::FIELD_IMAGE);
    }

    public function setImage($image): self
    {
        $this->setData(OfflineStoreEntity::FIELD_IMAGE, $image);
        return $this;
    }

    public function getEmployeeQty(): ?int
    {
        $employeeQty = $this->_get(OfflineStoreEntity::FIELD_EMPLOYEE_QTY);
        return $employeeQty ? (int) $employeeQty : null;
    }

    public function setEmployeeQty($employeeQty): self
    {
        $this->setData(OfflineStoreEntity::FIELD_EMPLOYEE_QTY, (int) $employeeQty);
        return $this;
    }

    public function getAverageSalary(): ?int
    {
        $averageSalary = $this->_get(OfflineStoreEntity::FIELD_AVERAGE_SALARY);
        return $averageSalary ? (int) $averageSalary : null;
    }

    public function setAverageSalary($averageSalary): self
    {
        $this->setData(OfflineStoreEntity::FIELD_AVERAGE_SALARY, (int) $averageSalary);
        return $this;
    }

    public function getCity(): string
    {
        return $this->_get(OfflineStoreEntity::FIELD_CITY);
    }

    public function setCity($city): self
    {
        $this->setData(OfflineStoreEntity::FIELD_CITY, $city);
        return $this;
    }

    public function getLat(): ?float
    {
        $lat = $this->_get(OfflineStoreEntity::FIELD_LAT);
        return $lat ? (float) $lat : null;
    }

    public function setLat($lat): self
    {
        $this->setData(OfflineStoreEntity::FIELD_LAT, (float) $lat);
        return $this;
    }

    public function getLng(): ?float
    {
        $lng = $this->_get(OfflineStoreEntity::FIELD_LNG);
        return $lng ? (float) $lng : null;
    }

    public function setLng($lng): self
    {
        $this->setData(OfflineStoreEntity::FIELD_LNG, (float) $lng);
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->_get(OfflineStoreEntity::FIELD_CREATED_AT);
    }

    public function setCreatedAt($createdAt): self
    {
        $this->setData(OfflineStoreEntity::FIELD_CREATED_AT, $createdAt);
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->_get(OfflineStoreEntity::FIELD_UPDATE_AT);
    }

    public function setUpdatedAt($updatedAt = null): self
    {
        $this->setData(OfflineStoreEntity::FIELD_UPDATE_AT, $updatedAt);
        return $this;
    }
}
