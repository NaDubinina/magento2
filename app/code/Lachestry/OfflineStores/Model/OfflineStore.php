<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * @method int     getAverageSalary()
 * @method int     getId()
 * @method int     getStoreId()
 * @method string  getTitle()
 * @method string  getMetaTitle()
 * @method string  getSlug()
 * @method string  getImage()
 * @method string  getEmployeeQty()
 * @method float   getLat()
 * @method float   getLng()
 * @method string  getCity()
 * @method OfflineStore getOfflineStore()
 * @method string  getCreatedAt()
 * @method string  getUpdatedAt()
 */

class OfflineStore extends AbstractModel
{
    public const FIELD_ID = 'id';
    public const FIELD_STORE_ID = 'store_id';
    public const FIELD_TITLE = 'title';
    public const FIELD_META_TITLE = 'meta_title';
    public const FIELD_SLUG = 'slug';
    public const FIELD_IMAGE = 'image';
    public const FIELD_EMPLOYEE_QTY = 'employee_qty';
    public const FIELD_AVERAGE_SALARY = 'average_salary';
    public const FIELD_CITY = 'city';
    public const FIELD_LAT = 'lat';
    public const FIELD_LNG = 'lng';
    public const FIELD_CREATED_AT = 'created_at';
    public const FIELD_UPDATE_AT = 'updated_at';
    public const FIELD_IS_ACTIVE = 'is_active';

    protected $_eventPrefix = 'lachestry_offline_store';
    protected $_eventObject = 'offline_store';

    protected function _construct()
    {
        $this->_init(\Lachestry\OfflineStores\Model\ResourceModel\OfflineStore::class);
    }
}
