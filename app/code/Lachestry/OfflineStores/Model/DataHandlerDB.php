<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model;

use Magento\Framework\Setup\ModuleDataSetupInterface;

class DataHandlerDB
{
    protected $setup;

    public function __construct(ModuleDataSetupInterface $setup)
    {
        $this->setup = $setup;
    }

    public function updateDatabaseArrayEntityData($tableName, $data): bool
    {
        $data = $this->setIdForData($data);
        return (bool) $this->setup->getConnection()->insertOnDuplicate($tableName, $data);
    }

    private function setIdForData($data)
    {
        if (!array_is_list($data)) {
            $data = [$data];
        }

        foreach ($data as &$entityData) {
            if (!array_key_exists('id', $entityData) || !((int) $entityData['id'])) {
                $entityData['id'] = NULL;
                continue;
            }
        }

        return $data;
    }
}