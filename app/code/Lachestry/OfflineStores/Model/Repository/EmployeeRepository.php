<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\Repository;

use Lachestry\OfflineStores\Api\Data\EmployeeInterface as DataEmployee;
use Lachestry\OfflineStores\Api\Data\EmployeeInterfaceFactory as DataEmployeeFactory;
use Lachestry\OfflineStores\Api\Data\EmployeeSearchResultInterface as EmployeeSearchResult;
use Lachestry\OfflineStores\Api\Data\EmployeeSearchResultInterfaceFactory as EmployeeSearchResultFactory;
use Lachestry\OfflineStores\Api\EmployeeRepositoryInterface;
use Lachestry\OfflineStores\Model\Employee;
use Lachestry\OfflineStores\Model\EmployeeFactory;
use Lachestry\OfflineStores\Model\ResourceModel\Employee as ResourceEmployee;
use Lachestry\OfflineStores\Model\ResourceModel\Employee\Collection as EmployeeCollection;
use Lachestry\OfflineStores\Model\ResourceModel\Employee\CollectionFactory as EmployeeCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    protected $employeeResource;
    protected $employeeFactory;
    protected $dataEmployeeFactory;
    protected $employeeCollectionFactory;
    protected $searchResultsFactory;
    protected $dataObjectHelper;
    protected $dataObjectProcessor;
    protected $storeManager;
    protected $collectionProcessor;

    public function __construct(
        ResourceEmployee $employeeResource,
        EmployeeFactory $employeeFactory,
        DataEmployeeFactory $dataEmployeeFactory,
        EmployeeCollectionFactory $employeeCollectionFactory,
        EmployeeSearchResultFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->employeeResource = $employeeResource;
        $this->employeeFactory = $employeeFactory;
        $this->dataEmployeeFactory = $dataEmployeeFactory;
        $this->employeeCollectionFactory = $employeeCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(\Lachestry\OfflineStores\Model\Data\Employee $employee): DataEmployee
    {
        try {
            $employee = $employee->setUpdatedAt();
            $employeeEntity = $this->createEmployeeEntity()->setData($employee->__toArray());
            $this->employeeResource->save($employeeEntity);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $employee;
    }

    public function getById($employeeId): DataEmployee
    {
        $employeeEntity = $this->createEmployeeEntity();
        $this->employeeResource->load($employeeEntity, $employeeId);

        if (!$employeeEntity->getId()) {
            throw new NoSuchEntityException(__('Employee with id "%1" does not exist.', $employeeId));
        }

        $employeeData = $this->buildEmployeeData($employeeEntity);

        return $employeeData;
    }

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria): EmployeeSearchResult
    {
        $collection = $this->createEmployeeCollection();

        $this->collectionProcessor->process($criteria, $collection);

        $employeeEntities = $collection->getItems();
        $dataEmployees = [];
        foreach ($employeeEntities as $employee) {
            $dataEmployees[] = $this->buildEmployeeData($employee);
        }

        $searchResults = $this->createSearchResult();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($dataEmployees);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    public function delete(\Lachestry\OfflineStores\Model\Data\Employee $employee): bool
    {
        try {
            $employeeEntity = $this->createEmployeeEntity()->setData($employee->__toArray());
            $this->employeeResource->delete($employeeEntity);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($employeeId): bool
    {
        try {
            $employee = $this->getById($employeeId);
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    private function createEmployeeEntity(): Employee
    {
        return  $this->employeeFactory->create();
    }

    private function createEmployeeData(): DataEmployee
    {
        return  $this->dataEmployeeFactory->create();
    }

    private function createEmployeeCollection(): EmployeeCollection
    {
        return  $this->employeeCollectionFactory->create();
    }

    private function createSearchResult(): EmployeeSearchResult
    {
        return  $this->searchResultsFactory->create();
    }

    private function buildEmployeeData($employeeModel): DataEmployee
    {
        $employeeData = $this->createEmployeeData();
        foreach ($employeeModel->getData() as $field => $value) {
            $employeeData->setData($field, $value);
        }

        return $employeeData;
    }
}
