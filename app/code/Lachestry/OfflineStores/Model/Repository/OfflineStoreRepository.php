<?php
declare(strict_types = 1);

namespace Lachestry\OfflineStores\Model\Repository;

use Lachestry\OfflineStores\Api\Data\OfflineStoreInterface as DataOfflineStore;
use Lachestry\OfflineStores\Api\Data\OfflineStoreInterfaceFactory as DataOfflineStoreFactory;
use Lachestry\OfflineStores\Api\Data\OfflineStoreSearchResultInterface as OfflineStoreSearchResult;
use Lachestry\OfflineStores\Api\Data\OfflineStoreSearchResultInterfaceFactory as OfflineStoreSearchResultFactory;
use Lachestry\OfflineStores\Api\OfflineStoreRepositoryInterface;
use Lachestry\OfflineStores\Model\OfflineStore;
use Lachestry\OfflineStores\Model\OfflineStoreFactory;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore as ResourceOfflineStore;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\Collection as OfflineStoreCollection;
use Lachestry\OfflineStores\Model\ResourceModel\OfflineStore\CollectionFactory as OfflineStoreCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class OfflineStoreRepository implements OfflineStoreRepositoryInterface
{
    protected $offlineStoreResource;
    protected $offlineStoreFactory;
    protected $dataOfflineStoreFactory;
    protected $offlineStoreCollectionFactory;
    protected $searchResultsFactory;
    protected $dataObjectHelper;
    protected $dataObjectProcessor;
    protected $storeManager;
    protected $collectionProcessor;
    protected $events;

    public function __construct(
        ResourceOfflineStore $offlineStoreResource,
        OfflineStoreFactory $offlineStoreFactory,
        DataOfflineStoreFactory $dataOfflineStoreFactory,
        OfflineStoreCollectionFactory $offlineStoreCollectionFactory,
        OfflineStoreSearchResultFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        ManagerInterface $events
    ) {
        $this->events = $events;
        $this->offlineStoreResource = $offlineStoreResource;
        $this->offlineStoreFactory = $offlineStoreFactory;
        $this->dataOfflineStoreFactory = $dataOfflineStoreFactory;
        $this->offlineStoreCollectionFactory = $offlineStoreCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(\Lachestry\OfflineStores\Model\Data\OfflineStore $offlineStore): DataOfflineStore
    {
        try {
            $offlineStore = $offlineStore->setUpdatedAt();

            if(empty($offlineStore->getStoreId())) {
                $offlineStore = $offlineStore->setStoreId($this->storeManager->getStore()->getId());
            }

            $offlineStoreEntity = $this->createOfflineStoreEntity()->setData($offlineStore->__toArray());
            $this->offlineStoreResource->save($offlineStoreEntity);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $offlineStore;
    }

    public function getById($offlineStoreId): DataOfflineStore
    {
        $offlineStoreEntity = $this->createOfflineStoreEntity();
        $this->offlineStoreResource->load($offlineStoreEntity, $offlineStoreId);

        if (!$offlineStoreEntity->getId()) {
            throw new NoSuchEntityException(__('Offline Store with id "%1" does not exist.', $offlineStoreId));
        }

        $offlineStoreData = $this->buildOfflineStoreData($offlineStoreEntity);

        return $offlineStoreData;
    }

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria): OfflineStoreSearchResult
    {
        $collection = $this->createOfflineStoreCollection();

        $this->collectionProcessor->process($criteria, $collection);

        $offlineStoreEntities = $collection->getItems();
        $dataOfflineStores = [];
        foreach ($offlineStoreEntities as $offlineStore) {
            $dataOfflineStores[] = $this->buildOfflineStoreData($offlineStore);
        }

        $searchResults = $this->createSearchResult();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($dataOfflineStores);
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    public function delete(\Lachestry\OfflineStores\Model\Data\OfflineStore $offlineStore): bool
    {
        try {
            $offlineStoreEntity = $this->createOfflineStoreEntity()->setData($offlineStore->__toArray());
            $this->offlineStoreResource->delete($offlineStoreEntity);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($offlineStoreId): bool
    {
        try {
            $offlineStore = $this->getById($offlineStoreId);

            if ($this->delete($offlineStore)) {
                $this->events->dispatch(
                    'lachestry_offline_store_delete',
                    ['offlineStore' => $offlineStore]
                );
            }

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    private function createOfflineStoreEntity(): OfflineStore
    {
        return  $this->offlineStoreFactory->create();
    }

    private function createOfflineStoreData(): DataOfflineStore
    {
        return  $this->dataOfflineStoreFactory->create();
    }

    private function createOfflineStoreCollection(): OfflineStoreCollection
    {
        return  $this->offlineStoreCollectionFactory->create();
    }

    private function createSearchResult(): OfflineStoreSearchResult
    {
        return  $this->searchResultsFactory->create();
    }

    private function buildOfflineStoreData($offlineStoreModel): DataOfflineStore
    {
        $offlineStoreData = $this->createOfflineStoreData();
        foreach ($offlineStoreModel->getData() as $field => $value) {
            $offlineStoreData->setData($field, $value);
        }

        return $offlineStoreData;
    }
}
