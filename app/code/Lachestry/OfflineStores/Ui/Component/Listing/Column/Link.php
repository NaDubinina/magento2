<?php

namespace Lachestry\OfflineStores\Ui\Component\Listing\Column;

use Lachestry\OfflineStores\Model\OfflineStore;

class Link extends \Magento\Catalog\Ui\Component\Listing\Columns\Thumbnail
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $entityLink = $this->urlBuilder->getRouteUrl('offlinestores') . $item[OfflineStore::FIELD_SLUG];
                $entityLink = str_replace('/index.php/admin', '', $entityLink);
                $item['link'] = "<a href='{$entityLink}'>{$entityLink}</a>";
            }
        }

        return $dataSource;
    }
}
