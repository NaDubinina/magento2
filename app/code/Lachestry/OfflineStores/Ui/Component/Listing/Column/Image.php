<?php

namespace Lachestry\OfflineStores\Ui\Component\Listing\Column;

use \Magento\Framework\UrlInterface;

class Image extends \Magento\Catalog\Ui\Component\Listing\Columns\Thumbnail
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if(isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            $baseImageUrl = $this->urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]);
            foreach($dataSource['data']['items'] as &$item) {
                if ($item['image']) {
                    $imageUrl = $baseImageUrl . $item['image'];
                    $item[$fieldName . '_alt'] = $this->getAlt($item) ?: $item['image'];
                    $item[$fieldName . '_src'] = $imageUrl;
                    $item[$fieldName . '_link'] = $imageUrl;
                    $item[$fieldName] = $imageUrl;
                    $item[$fieldName . '_orig_src'] = $imageUrl;
                } else {
                    $item[$fieldName . '_src'] = '';
                    $item[$fieldName . '_link'] = '';
                    $item[$fieldName] = '';
                }
            }
        }

        return $dataSource;
    }
}