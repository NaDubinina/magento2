<?php
declare(strict_types=1);

namespace Lachestry\OfflineStores\Ui\Component\Listing\Column;

use Lachestry\OfflineStores\Model\OfflineStore;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

class EditAction extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $urlBuilder;
    public const URL_PATH_EDIT = 'offlinestores/offlinestore/edit';
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @inheritDoc
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item[OfflineStore::FIELD_ID])) {
                    $item[$this->getData('name')] = [
                        'edit' => [
                            'href' => $this->urlBuilder->getUrl(
                                static::URL_PATH_EDIT,
                                [
                                    OfflineStore::FIELD_ID => $item[OfflineStore::FIELD_ID],
                                ]
                            ),
                            'label' => __('Edit'),
                        ]
                    ];
                }
            }
        }

        return $dataSource;
    }
}
