<?php
declare(strict_types = 1);

namespace Lachestry\Telegram\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class TelegramConfig
{
    public const TELEGRAM_METHOD_GET_UPDATES = 'getUpdates';
    public const TELEGRAM_METHOD_SEND_MASSAGE = 'sendMessage';

    private $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getToken()
    {
        return $this->scopeConfig->getValue('lachestry_telegram/bot/token');
    }

    public function getChatId()
    {
        return $this->scopeConfig->getValue('lachestry_telegram/general/chat_id');
    }

    public function getTelegramApi()
    {
        return $this->scopeConfig->getValue('lachestry_telegram/bot/api');
    }
}
