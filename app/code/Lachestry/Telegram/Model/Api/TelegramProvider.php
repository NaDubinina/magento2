<?php
declare(strict_types = 1);

namespace Lachestry\Telegram\Model\Api;

use Lachestry\Telegram\Model\TelegramConfig;
use Lachestry\Telegram\Model\Api\Http\Client;
use Magento\Framework\HTTP\ZendClient;

class TelegramProvider
{
    protected $client;
    protected $config;

    public function __construct(Client $client, TelegramConfig $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    public function getMessages(): array
    {
        try {
            return $this->client->request($this->prepareUrl(TelegramConfig::TELEGRAM_METHOD_GET_UPDATES));
        } catch (\Exception $e) {
            return [];
        }
    }

    public function sendMessage(string $message = '', string $chatId = null): array
    {
        if (!$chatId) {
            $chatId = $this->config->getChatId();
        }

        return $this->client
            ->setBodyData([
                'chat_id' => $chatId,
                'text' => $message,
                'parse_mode' => 'html'
            ])
            ->request($this->prepareUrl(TelegramConfig::TELEGRAM_METHOD_SEND_MASSAGE), ZendClient::POST);
    }


    public function prepareUrl(string $method): string
    {
        $url = $this->config->getTelegramApi() . $this->config->getToken();

        return $method ? ($url . '/' . $method) : $url;
    }
}
